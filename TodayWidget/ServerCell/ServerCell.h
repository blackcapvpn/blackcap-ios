//
//  ServerCell.h
//  Blackcap
//
//  Created by rstar on 6/30/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivFlag;
@property (weak, nonatomic) IBOutlet UILabel *lblServerName;
@property (weak, nonatomic) IBOutlet UIButton *btnSetup;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
