//
//  ServerCell.m
//  Blackcap
//
//  Created by rstar on 6/30/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "ServerCell.h"

@implementation ServerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
