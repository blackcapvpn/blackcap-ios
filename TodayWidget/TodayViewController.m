//
//  TodayViewController.m
//  TodayWidget
//
//  Created by rstar on 6/30/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "ApiManager.h"
#import "Constant.h"
#import "BlackCapServer.h"
#import "UIViewController+Alert.h"
#import "ServerCell.h"

@interface TodayViewController () <NCWidgetProviding, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *serverTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblNoServer;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentServer;
@property (weak, nonatomic) IBOutlet UISwitch *switchConnect;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorConnecting;
@property (weak, nonatomic) IBOutlet UILabel *lblServerIP;
@property (weak, nonatomic) IBOutlet UILabel *lblConnectedTime;
@property (weak, nonatomic) IBOutlet UILabel *lblLeftDays;
@property (weak, nonatomic) IBOutlet UILabel *lblPingTime;

@property (nonatomic, strong) BlackCapVPNManager *vpnManager;
@property (nonatomic, strong) BlackCapServer *configuredServer;
@property (nonatomic, strong) NSMutableArray *serverList;
@property (nonatomic, strong) NSTimer *myTimer;
@property (nonatomic, strong) NSMutableDictionary *pingTimes;
@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initView];
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData
    
    completionHandler(NCUpdateResultNewData);
}

- (void)widgetActiveDisplayModeDidChange:(NCWidgetDisplayMode)activeDisplayMode withMaximumSize:(CGSize)maxSize {
    if (activeDisplayMode == NCWidgetDisplayModeExpanded) {
        NSInteger height = 40 * self.serverList.count + 110;
        self.preferredContentSize = CGSizeMake(0, height);
    } else if (activeDisplayMode == NCWidgetDisplayModeCompact) {
        self.preferredContentSize = CGSizeMake(0, 110);
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [self.vpnManager loadOpenVPNTunnelProviderManager:^(BOOL success, NSString *returnInfo) {
        if (success) {
            [self.vpnManager mp_NEVPNStatusChanged:^(enum NEVPNStatus status) {
                [self showConnectionStatus:status];
            }];
            [self showConnectionStatus:self.vpnManager.status];
            [self showConfiguredServer];
            [self.serverTableView reloadData];
        } else {
            NSLog(@"%@", returnInfo);
        }
    }];
    [self startTimer];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.vpnManager) {
        [self.vpnManager mp_RemoveNEVPNStatusChangedNotification];
    }
    [self stopTimer];
}

- (void)initView {
    self.vpnManager = [BlackCapVPNManager shareInstance];
    self.serverList = [MyUtils loadCurrentWorkingServers:USERDEFAULTS_CURRENT_WORKING_SERVERS];

    self.extensionContext.widgetLargestAvailableDisplayMode = NCWidgetDisplayModeExpanded;
    self.pingTimes = [kUserDefaults objectForKey:USERDEFAULTS_PING_TIMES];
}

- (void)showConfiguredServer {
    _configuredServer = [MyUtils loadCustomObjectWithKey:USERDEFAULTS_CONFIGURED_SERVER];
    if (_configuredServer) {
        //[BlackCapVPNCommon insertLogIntoDataBaseWithLog:[NSString stringWithFormat:@"OpenVPN connected to the server - %@ - %@", _configuredServer.country, _configuredServer.ip]];
        
        self.lblCurrentServer.text = _configuredServer.name;
        self.lblServerIP.text = _configuredServer.ip;
        self.lblConnectedTime.text = [self.vpnManager getConnectionTime];
//        self.lblDownSpeed.text = @"0.0KB/S";
//        self.lblDownSize.text = @"0.0KB";
//        self.lblUpSpeed.text = @"0.0KB/S";
//        self.lblUpSize.text = @"0.0KB";
        self.lblPingTime.text = [self.pingTimes objectForKey:_configuredServer.ip];
    } else {
        self.lblCurrentServer.text = @"-";
        self.lblServerIP.text = @"-";
        self.lblConnectedTime.text = @"00:00";
//        self.lblDownSpeed.text = @"0.0KB/S";
//        self.lblDownSize.text = @"0.0KB";
//        self.lblUpSpeed.text = @"0.0KB/S";
//        self.lblUpSize.text = @"0.0KB";
        self.lblPingTime.text = @"0";
    }
}

- (IBAction)switchToggled:(id)sender {
    if (![kUserDefaults boolForKey:USERDEFAULTS_LOGIN_STATUS_VPN]) {
        [self openMainApp];
        return;
    }
    
    if ([sender isOn]) {
        [self.vpnManager start];
    } else {
        [self.vpnManager stop];
    }
}

- (void)openMainApp {
    NSURL *url = [NSURL URLWithString:@"blackcap://connect"];
    [self.extensionContext openURL:url completionHandler:nil];
}

#pragma mark - Tableview cell setup button clicked
- (void)btnSetupClicked:(UIButton*)sender {
    [MyUtils saveCustomObject:[self.serverList objectAtIndex:sender.tag] key:USERDEFAULTS_TODAY_CONFIGURING_SERVER];
    [self openMainApp];
    
}

- (void)configureOpenVPN:(BlackCapServer*)server {
    BlackCapVPNOpenVPNConfig *config = [BlackCapVPNOpenVPNConfig new];
    config.username = [kUserDefaults objectForKey:USERDEFAULTS_USERNAME];
    config.password = [kUserDefaults objectForKey:USERDEFAULTS_PASSWORD];

    [[self vpnManager] setConfig:config andServer:server];
    [[self vpnManager] saveConfigCompleteHandle:^(BOOL success, NSString *returnInfo) {
        if (success) {
            NSLog(@"config OpenVPN success");
            [MyUtils saveCustomObject:server key:USERDEFAULTS_CONFIGURED_SERVER];
            
            [self showConfiguredServer];
            [self.serverTableView reloadData];
        }
        else
        {
            NSLog(@"config OpenVPN error:%@", returnInfo);
        }
    }];
}

#pragma mark - Timer methods
- (void)startTimer {
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(updateWidget:)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)stopTimer {
    [self.myTimer invalidate];
    self.myTimer = nil;
}

- (void)updateWidget:(NSTimer*)timer {
    self.lblConnectedTime.text = [self.vpnManager getConnectionTime];
//    self.lblDownSpeed.text = @"0.0KB/S";
//    self.lblDownSize.text = @"0.0KB";
//    self.lblUpSpeed.text = @"0.0KB/S";
//    self.lblUpSize.text = @"0.0KB";
}

#pragma mark - self methods
- (void)showConnectionStatus:(NEVPNStatus)status {
    switch (status) {
        case NEVPNStatusInvalid:
            [self.switchConnect setHidden:NO];
            [self.switchConnect setOn:NO];
            [self.serverTableView setUserInteractionEnabled:YES];
            NSLog(@"Today-NEVPNStatusInvalid The VPN is not configured.");
            break;
        case NEVPNStatusDisconnected:
            [self.switchConnect setHidden:NO];
            [self.switchConnect setOn:NO];
            [self.serverTableView setUserInteractionEnabled:YES];
            NSLog(@"Today-NEVPNStatusDisconnected The VPN is disconnected.");
            break;
        case NEVPNStatusConnecting:
            [self.switchConnect setHidden:YES];
            [self.serverTableView setUserInteractionEnabled:NO];
            NSLog(@"Today-NEVPNStatusConnecting The VPN is connecting.");
            break;
        case NEVPNStatusConnected:
            [self.switchConnect setHidden:NO];
            [self.switchConnect setOn:YES];
            [self.serverTableView setUserInteractionEnabled:NO];
            NSLog(@"Today-NEVPNStatusConnected The VPN is connected.");
            break;
        case NEVPNStatusReasserting:
            [self.switchConnect setHidden:YES];
            [self.serverTableView setUserInteractionEnabled:NO];
            NSLog(@"Today-NEVPNStatusReasserting The VPN is reconnecting following loss of underlying network connectivity.");
            break;
        case NEVPNStatusDisconnecting:
            [self.switchConnect setHidden:YES];
            [self.serverTableView setUserInteractionEnabled:NO];
            NSLog(@"Today-NEVPNStatusDisconnecting The VPN is disconnecting.");
            break;
        default:
            break;
    }
}

#pragma mark - Call APIs
- (void)callServerList {
    [ApiManager getDataFromURL:SUB_URL_SERVER_LIST withParameter:nil authToken:nil sender:nil responseHandler:^(id  _Nullable responseObject, bool isSuccess) {
        if ([[[responseObject objectForKey:@"meta"] objectForKey:@"code"] intValue] == 200) {
            self.serverList = [[NSMutableArray alloc] init];
            id workers = [[responseObject objectForKey:@"data"] objectForKey:@"workers"];
            if ([workers count] > 0) {
                for (id worker in workers) {
                    BlackCapServer *server = [[BlackCapServer alloc] initWithDictionary:worker];
                    if (server.up) {
                        [self.serverList addObject:server];
                    }
                }
                [self.serverTableView reloadData];
            } else {
                [self.serverTableView setHidden:YES];
                [self.lblNoServer setHidden:NO];
            }
        } else {
        }
    }];
}

#pragma mark - tableview datasource and delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.serverList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"ServerCell";
    ServerCell *cell=(ServerCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"ServerCell" owner:nil options:nil];
        cell=[nib objectAtIndex:0];
    }
    
    BlackCapServer *server = [self.serverList objectAtIndex:indexPath.row];
    if (_configuredServer) {
        if ([server.name isEqualToString:_configuredServer.name]) {
            [cell.btnSetup setHidden:YES];
        }
    }
    cell.lblServerName.text = server.name;
    [cell.ivFlag setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [server.country lowercaseString]]]];
    
    cell.btnSetup.tag = indexPath.row;
    [cell.btnSetup addTarget:self action:@selector(btnSetupClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:210.0/255.0 blue:200.0/255.0 alpha:0.3];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.backgroundColor = (__bridge CGColorRef _Nullable)([UIColor clearColor]);//optional
    cell.backgroundView = nil;
}

#pragma mark - methods for ping times
- (void)startPingTimes {
    NSMutableArray *serverList = [[NSMutableArray alloc] initWithArray:[MyUtils loadCurrentWorkingServers:USERDEFAULTS_CURRENT_WORKING_SERVERS]];
    PingTime *pingTime = [[PingTime alloc] init];
    for (BlackCapServer *server in serverList) {
        [pingTime createPinger:server.ip];
    }
}

- (NSString*)getPingTimeForAddress:(NSString*)ipAddr {
    NSMutableDictionary *pingTimes = [kUserDefaults objectForKey:USERDEFAULTS_PING_TIMES];
    NSString *pingTime = [pingTimes objectForKey:ipAddr];
    return pingTime;
}
@end

