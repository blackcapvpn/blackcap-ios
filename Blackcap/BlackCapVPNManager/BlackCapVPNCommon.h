//
//  BlackCapVPNCommon.h
//  BlackCapVPNManager
//
//  Created by rstar on 2017/6/15.
//  Copyright © 2017. rstar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BlackCapVPNCommon : NSObject

extern NSString * const kBlackCapVPNConfigKey;

+ (NSUserDefaults *)sharedUserDefaults;

+ (BOOL)saveConfig:(id)config;

+ (id)getConfig;


extern NSString * const BlackCapVPNPasswordIdentifier;
extern NSString * const BlackCapVPNSharePrivateKeyIdentifier;

+ (BOOL)createKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier;

+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier;

// Realm Database insert and delete for VPN Log
+ (void)insertLogIntoDataBaseWithLog:(NSString *)log;
+ (void)deleteDatabase;

@end
