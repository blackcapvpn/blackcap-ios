//
//  ViewController.h
//  BlackCapVPNManager
//
//  Created by rstar on 2017/6/30.
//  Copyright © 2017. rstar. All rights reserved.
//


#import <Foundation/Foundation.h>

#import "BlackCapVPNCommon.h"
#import "BlackCapServer.h"

#import "BlackCapVPNConfig.h"
#import "BlackCapVPNIKEv2Config.h"
#import "BlackCapVPNIPSecConfig.h"
#import "BlackCapVPNL2TPConfig.h"
#import "BlackCapVPNOpenVPNConfig.h"
#import "PingTime.h"

typedef void(^CompleteHandle)(BOOL success , NSString * returnInfo);
typedef void(^StatusChanged)(enum NEVPNStatus status);

typedef NS_ENUM(NSInteger, BlackCapVPNManagerType){
    BlackCapVPNManagerTypeNone,
    BlackCapVPNManagerTypeIPSec,
    BlackCapVPNManagerTypeIKEv2,
    BlackCapVPNManagerTypeL2TP,
    BlackCapVPNManagerTypeOpenVPN,
};

@class BlackCapVPNConfig;
@interface BlackCapVPNManager : NSObject

+ (instancetype)shareInstance;
- (void)loadOpenVPNTunnelProviderManager:(CompleteHandle)completeHandle;

/** config info */
@property (nonatomic, readonly, strong) BlackCapVPNConfig *config;
@property (nonatomic, readonly, strong) BlackCapServer *server;
@property (nonatomic, readonly, assign) BlackCapVPNManagerType vpnType;

- (void)setConfig:(BlackCapVPNConfig *)config andServer:(BlackCapServer*)server;

/** run status */
@property (nonatomic, readonly, assign) enum NEVPNStatus status;

/** save config */
- (void)saveConfigCompleteHandle:(CompleteHandle)completeHandle;

- (void)start;
- (void)stop;
- (NSString*)getConnectionTime;
- (NSString*)getConnectedServerIP;

- (void)mp_NEVPNStatusChanged:(StatusChanged)statusChanged;
- (void)mp_RemoveNEVPNStatusChangedNotification;

@end

