
#import "BlackCapVPNManager.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import "NEVPNProtocolL2TP.h"
#import "BlackCapVPNCommon.h"
#include <dlfcn.h>

//#import "AFHTTPRequestOperation.h"

//static NSString * const BlackCapVPNPasswordIdentifier = @"BlackCapVPNPasswordIdentifier"; //
//static NSString * const BlackCapVPNSharePrivateKeyIdentifier = @"BlackCapVPNSharePrivateKeyIdentifier"; //

@interface BlackCapVPNManager ()
{
    BOOL bStopbyUser;
}
@property (nonatomic, strong) NEVPNManager * vpnManager;
@property (nonatomic, strong) id observer;

@end

@implementation BlackCapVPNManager

+ (instancetype)shareInstance
{
    static id instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.vpnManager = [NEVPNManager sharedManager];
        _vpnType = BlackCapVPNManagerTypeOpenVPN;
        bStopbyUser = NO;
        [self performSelector:@selector(registerNetWorkReachability) withObject:nil afterDelay:0.35f];
    }
    return self;
}

- (void)loadOpenVPNTunnelProviderManager:(CompleteHandle)completeHandle {
    [NETunnelProviderManager loadAllFromPreferencesWithCompletionHandler:^(NSArray<NETunnelProviderManager *> * _Nullable managers, NSError * _Nullable error) {
        if (error) {
            completeHandle(NO, [NSString stringWithFormat:@"load all error-%@", error.localizedDescription]);
            return;
        }
        
        if (managers.firstObject) {
            self.vpnManager = (NETunnelProviderManager*)managers.firstObject;
        } else {
            self.vpnManager = [[NETunnelProviderManager alloc] init];
        }
        completeHandle(YES, @"Successfully loaded!");
    }];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (enum NEVPNStatus)status{
    return _vpnManager.connection.status;
}

- (void)setConfig:(BlackCapVPNConfig *)config andServer:(BlackCapServer *)server
{
    if ([config isKindOfClass:[BlackCapVPNIPSecConfig class]]) {
        _config = config;
        _server = server;
        _vpnType = BlackCapVPNManagerTypeIPSec;
    } else if ([config isKindOfClass:[BlackCapVPNIKEv2Config class]]){
        _config = config;
        _server = server;
        _vpnType = BlackCapVPNManagerTypeIKEv2;
    } else if ([config isKindOfClass:[BlackCapVPNL2TPConfig class]]){
        _config = config;
        _server = server;
        _vpnType = BlackCapVPNManagerTypeL2TP;
    } else if ([config isKindOfClass:[BlackCapVPNOpenVPNConfig class]]) {
        _config = config;
        _server = server;
        _vpnType = BlackCapVPNManagerTypeOpenVPN;
    }
    else {
        _config = nil;
        _vpnType = BlackCapVPNManagerTypeNone;
    }
   
}

- (void) createKeychainPassword:(NSString *)password privateKey:(NSString *)privateKey
{
    if (password.length) {
         [BlackCapVPNCommon createKeychainValue:password forIdentifier:BlackCapVPNPasswordIdentifier];
    }
   
    if (privateKey.length) {
        [BlackCapVPNCommon createKeychainValue:privateKey forIdentifier:BlackCapVPNSharePrivateKeyIdentifier];
    }
   
}

- (void) saveIPSec:(CompleteHandle)completeHandle {
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError *error) {
        if (error) {
            completeHandle(NO, [NSString stringWithFormat:@"Load config failed [%@]", error.localizedDescription]);
            return;
        }
        BlackCapVPNIPSecConfig *privateIPSecConfig = (BlackCapVPNIPSecConfig *)_config;
        NEVPNProtocolIPSec *p = (NEVPNProtocolIPSec*)_vpnManager.protocol;
        if (!p) {
            p = [[NEVPNProtocolIPSec alloc] init];
        }
        
        p.username = privateIPSecConfig.username;
        p.serverAddress = _server.ip;
        [BlackCapVPNCommon createKeychainValue:privateIPSecConfig.password forIdentifier:BlackCapVPNPasswordIdentifier];
        p.passwordReference = [BlackCapVPNCommon searchKeychainCopyMatching:BlackCapVPNPasswordIdentifier];
        
        [BlackCapVPNCommon createKeychainValue:privateIPSecConfig.sharePrivateKey forIdentifier:BlackCapVPNSharePrivateKeyIdentifier];
        if ([BlackCapVPNCommon searchKeychainCopyMatching:BlackCapVPNSharePrivateKeyIdentifier] &&
            privateIPSecConfig.sharePrivateKey) {
            p.authenticationMethod = NEVPNIKEAuthenticationMethodSharedSecret;
            p.sharedSecretReference = [BlackCapVPNCommon searchKeychainCopyMatching:BlackCapVPNSharePrivateKeyIdentifier];
        }
        else if (privateIPSecConfig.identityData && privateIPSecConfig.password) {
            p.authenticationMethod = NEVPNIKEAuthenticationMethodCertificate;
            p.identityData = privateIPSecConfig.identityData;
            p.identityDataPassword = privateIPSecConfig.identityDataPassword;
        }
        else{
            p.authenticationMethod = NEVPNIKEAuthenticationMethodNone;
        }
        
        p.localIdentifier = privateIPSecConfig.localIdentifier;
        p.remoteIdentifier = privateIPSecConfig.remoteIdentifier;
        p.useExtendedAuthentication = YES;
        p.disconnectOnSleep = NO;
        
        _vpnManager.protocol = p;
        _vpnManager.onDemandEnabled = YES;
        _vpnManager.localizedDescription = [NSString stringWithFormat:@"IPSec %@ %@", _server.country, _server.city];
        _vpnManager.enabled = YES;
        
        [_vpnManager saveToPreferencesWithCompletionHandler:^(NSError *error) {
            if(error) {
                completeHandle(NO,[NSString stringWithFormat:@"Save config failed [%@]", error.localizedDescription]);
            }
            else {
                [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                    if (error) {
                        completeHandle(NO,[NSString stringWithFormat:@"Load config failed [%@]", error.localizedDescription]);
                        return;
                    }
                    completeHandle(YES, @"Save config success");
                }];
            }
        }];
    }];
}

- (void) saveIKEv2:(CompleteHandle)completeHandle {
    [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            completeHandle(NO,[NSString stringWithFormat:@"Load config failed [%@]", error.localizedDescription]);
            return;
        }
        
        BlackCapVPNIKEv2Config *privateIKEv2Config = (BlackCapVPNIKEv2Config *)_config;
        
        NEVPNProtocolIKEv2 *p = [NEVPNProtocolIKEv2 new];
        p.username = privateIKEv2Config.username;
        p.passwordReference = [BlackCapVPNCommon searchKeychainCopyMatching:BlackCapVPNPasswordIdentifier];
        
        p.serverAddress = _server.ip;
        p.serverCertificateIssuerCommonName = privateIKEv2Config.serverCertificateCommonName;
        p.serverCertificateCommonName = privateIKEv2Config.serverCertificateCommonName;
        
        if ([BlackCapVPNCommon searchKeychainCopyMatching:BlackCapVPNSharePrivateKeyIdentifier] &&
            privateIKEv2Config.sharePrivateKey) {
            p.authenticationMethod = NEVPNIKEAuthenticationMethodSharedSecret;
            p.sharedSecretReference = [BlackCapVPNCommon searchKeychainCopyMatching:BlackCapVPNSharePrivateKeyIdentifier];
        }
        else if (privateIKEv2Config.identityData && privateIKEv2Config.password) {
            p.authenticationMethod = NEVPNIKEAuthenticationMethodCertificate;
            p.identityData = privateIKEv2Config.identityData;
            p.identityDataPassword = privateIKEv2Config.identityDataPassword;
        }
        else{
            p.authenticationMethod = NEVPNIKEAuthenticationMethodNone;
        }
        
        //p.identityData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"point-to-client2" ofType:@"p12"]];
        //p.identityDataPassword = @"vpnuser";
        
        p.localIdentifier = privateIKEv2Config.localIdentifier;
        p.remoteIdentifier = privateIKEv2Config.remoteIdentifier;
        p.useExtendedAuthentication = YES;
        p.disconnectOnSleep = NO;
        
        _vpnManager.protocol = p;
        _vpnManager.onDemandEnabled = YES;
        _vpnManager.enabled = YES;
        _vpnManager.localizedDescription = [NSString stringWithFormat:@"IKEv2 %@ %@", _server.country, _server.city];
        
        //NEEvaluateConnectionRule * ru = [[NEEvaluateConnectionRule alloc]
        //                                 initWithMatchDomains:@[@"google.com"]
        //                                 andAction:NEEvaluateConnectionRuleActionConnectIfNeeded];
        //
        //ru.probeURL = [[NSURL alloc] initWithString:@"http://www.google.com"];
        //
        //NEOnDemandRuleEvaluateConnection *ec =[[NEOnDemandRuleEvaluateConnection alloc] init];
        ////                ec.interfaceTypeMatch = NEOnDemandRuleInterfaceTypeWiFi;
        //[ec setConnectionRules:@[ru]];
        //[_vpnManager setOnDemandRules:@[ec]];
        
        [_vpnManager saveToPreferencesWithCompletionHandler:^(NSError *error) {
            if(error) {
                completeHandle(NO,[NSString stringWithFormat:@"Save config failed [%@]", error.localizedDescription]);
            }
            else {
                [BlackCapVPNCommon saveConfig:_config];
                id conf = [BlackCapVPNCommon getConfig];
                [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                    if (error) {
                        completeHandle(NO,[NSString stringWithFormat:@"Load config failed [%@]", error.localizedDescription]);
                        return;
                    }
                    completeHandle(YES, @"Save config success");
                }];
            }
        }];
        
    }];
}

- (void)saveL2TP:(CompleteHandle)completeHandle {
    //NSBundle *b = [NSBundle bundleWithPath:@"/System/Library/Frameworks/NetworkExtension.framework"];
    //BOOL success = [b load];
    void *lib = dlopen("/System/Library/Frameworks/NetworkExtension.framework/NetworkExtension", RTLD_LAZY);
    if(lib) {
        [_vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
            
            Class NEVPNProtocolL2TP = NSClassFromString(@"NEVPNProtocolL2TP");
            if (NEVPNProtocolL2TP) {
                NSLog(@"NEVPNProtocolL2TP");
            }
            BlackCapVPNL2TPConfig *privateL2TPConfig = (BlackCapVPNL2TPConfig *)_config;
            NEVPNProtocol *p = [[NEVPNProtocolL2TP alloc] init];
            
            p.serverAddress = _server.ip;
            p.username = privateL2TPConfig.username;
            
            [BlackCapVPNCommon createKeychainValue:privateL2TPConfig.password forIdentifier:BlackCapVPNPasswordIdentifier];
            p.passwordReference = [BlackCapVPNCommon searchKeychainCopyMatching:BlackCapVPNPasswordIdentifier];
            
            [BlackCapVPNCommon createKeychainValue:privateL2TPConfig.sharePrivateKey forIdentifier:BlackCapVPNSharePrivateKeyIdentifier];
            [p performSelector:@selector(setSharedSecretKeychainItem:) withObject:[BlackCapVPNCommon searchKeychainCopyMatching:BlackCapVPNSharePrivateKeyIdentifier]];
            
            p.disconnectOnSleep = NO;
            
            
            _vpnManager.protocol = p;
            _vpnManager.onDemandEnabled = YES;
            _vpnManager.localizedDescription = [NSString stringWithFormat:@"L2TP %@ %@", _server.country, _server.city];
            _vpnManager.enabled = YES;
            
            NSLog(@"ok");
            
            [ [NEVPNManager sharedManager] saveToPreferencesWithCompletionHandler:^(NSError *error) {
                if(error) {
                    completeHandle(NO, [NSString stringWithFormat:@"Save config failed [%@]", error.localizedDescription]);
                }
                else {
                    [ [NEVPNManager sharedManager] loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                        if (error) {
                            completeHandle(NO, [NSString stringWithFormat:@"Load config failed [%@]", error.localizedDescription]);
                            return;
                        }
                        completeHandle(YES, nil);
                    }];
                }
            }];
        }];
    }
    
}

- (void)saveOpenVPN:(CompleteHandle)completeHandle {
    NSString *urlServerConfig = [NSString stringWithFormat:@"%@%@", SUB_URL_OVPN_CONFIG, _server.domainName];
    [ApiManager getStringFromURL:urlServerConfig withParameter:nil authToken:nil sender:nil responseHandler:^(id  _Nullable responseObject, bool isSuccess) {
        [self.vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
            if (error) {
                completeHandle(NO, [NSString stringWithFormat:@"load error-%@", error.localizedDescription]);
                return ;
            }
            BlackCapVPNOpenVPNConfig *openvpnConfig = (BlackCapVPNOpenVPNConfig *)_config;
            
            NETunnelProviderProtocol *tunnelProtocol = [[NETunnelProviderProtocol alloc] init];
            
//            NSString *configurationFileName = [NSString stringWithFormat:@"%@_blackcap_zone", [[_server.domainName componentsSeparatedByString:@"."] firstObject]];
//            NSURL *configurationFile = [[NSBundle mainBundle] URLForResource:configurationFileName withExtension:@"ovpn"];
//            NSData *configurationContent = [NSData dataWithContentsOfURL:configurationFile];
            
            NSData *configurationApi = [responseObject dataUsingEncoding:NSUTF8StringEncoding];
            
            tunnelProtocol.providerBundleIdentifier = TunnelProviderBundleID;
            tunnelProtocol.providerConfiguration = @{ ConfigurationKeyFileContent:configurationApi };
            
            tunnelProtocol.serverAddress = _server.ip;
            tunnelProtocol.username = openvpnConfig.username;
            
            [BlackCapVPNCommon createKeychainValue:openvpnConfig.password forIdentifier:BlackCapVPNPasswordIdentifier];
            tunnelProtocol.passwordReference = [BlackCapVPNCommon searchKeychainCopyMatching:BlackCapVPNPasswordIdentifier];
            
            tunnelProtocol.disconnectOnSleep = false;
            
            self.vpnManager.protocolConfiguration = tunnelProtocol;
            
            //Enable On-Demand functionality : Set on demand rules(establish vpn connection whenever to access the internet.)
            if ([kUserDefaults boolForKey:USERDEFAULTS_AUTO_RECONNECT]) {
                [self.vpnManager setOnDemandEnabled:YES];
                NSMutableArray *rules = [[NSMutableArray alloc] init];
                NEOnDemandRuleConnect *connectRule = [NEOnDemandRuleConnect new];
                [rules addObject:connectRule];
                [self.vpnManager setOnDemandRules:rules];
            }
            
            self.vpnManager.localizedDescription = [NSString stringWithFormat:@"BlackCap - %@", _server.name];
            
            [self.vpnManager setEnabled:YES];
            
            [self.vpnManager saveToPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                if (error) {
                    completeHandle(NO, [NSString stringWithFormat:@"save error-%@", error.localizedDescription]);
                    return ;
                }
                [self.vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                    if (error) {
                        completeHandle(NO, [NSString stringWithFormat:@"Load config failed [%@]", error.localizedDescription]);
                        return;
                    }
                    completeHandle(YES, @"Save config success");
                }];
            }];
        }];
    }];

}

- (void)disableOnDemand:(CompleteHandle)completeHandle {
    
    [self.vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            completeHandle(NO, [NSString stringWithFormat:@"load error-%@", error.localizedDescription]);
            return ;
        }
        [self.vpnManager setOnDemandEnabled:NO];
        
        [self.vpnManager saveToPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
            if (error) {
                completeHandle(NO, [NSString stringWithFormat:@"save error-%@", error.localizedDescription]);
                return ;
            }
            [self.vpnManager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                if (error) {
                    completeHandle(NO, [NSString stringWithFormat:@"Load config failed [%@]", error.localizedDescription]);
                    return;
                }
                completeHandle(YES, @"Save config success");
            }];
        }];
    }];
}

- (void) saveConfigCompleteHandle:(CompleteHandle)completeHandle {
    if (!_vpnManager) {
        completeHandle(NO, @"NEVPNManager Uninitialized");
        return;
    }
    
    if (!_config) {
        completeHandle(NO, @"Configuration parameters cannot be empty");
        return;
    }
    
    // 1. NEVPNManager
    // 2. loadFromPreferencesWithCompletionHandler
    // 3. saveToPreferencesWithCompletionHandler
    switch (_vpnType) {
        case BlackCapVPNManagerTypeIPSec:
            [self saveIPSec:completeHandle];
            break;
        case BlackCapVPNManagerTypeIKEv2:
            [self saveIKEv2:completeHandle];
            break;
        case BlackCapVPNManagerTypeL2TP:
            [self saveL2TP:completeHandle];
            break;
        case BlackCapVPNManagerTypeOpenVPN:
            [self saveOpenVPN:completeHandle];
            break;
        case BlackCapVPNManagerTypeNone:
            completeHandle(NO, @"please set config or vpn type.");
            break;
    }
}


#pragma mark - BEGIN
- (void)registerNetWorkReachability{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetWork) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    
}

-(void)checkNetWork{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusReachableViaWWAN ||
            status == AFNetworkReachabilityStatusReachableViaWiFi) {
            if (self.vpnManager.connection.status != NEVPNStatusConnected && !bStopbyUser) {
                [self start];
            } else {
                bStopbyUser = NO;
            }
        }
    }];
}

#pragma mark - END
- (void)start{
    NSError *startError;

    [self.vpnManager.connection startVPNTunnelWithOptions:nil andReturnError:&startError];
    
    if (startError) {
        NSLog(@"Start VPN failed: [%@]", startError.localizedDescription);
    }
}

- (void)stop{
    bStopbyUser = YES;
    [_vpnManager.connection stopVPNTunnel];
    
    [self disableOnDemand:^(BOOL success, NSString *returnInfo) {
        if (success) {
            NSLog(@"Disable On-Demand success");
        }
        else
        {
            NSLog(@"Disable On-Demand error:%@", returnInfo);
        }
    }];
    NSLog(@"VPN has stopped success");
}

- (NSString*)getConnectionTime {
    NSTimeInterval elapsedTime = [[NSDate date] timeIntervalSinceDate:[self.vpnManager.connection connectedDate]];
    
    return [MyUtils getElapsedTimeWithInterval:elapsedTime];
}

- (NSString*)getConnectedServerIP {
    return [self.vpnManager.protocolConfiguration serverAddress];
}
//- (void)testInternetSpeed {
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://download.thinkbroadband.com/1GB.zip"]];
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    operation.downloadSpeedMeasure.active = YES;
//    
//    // to avoid a retain cycle one has to pass a weak reference to operation into the progress block.
//    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
//        double speedInBytesPerSecond = operation.downloadSpeedMeasure.speed;
//        NSString *humanReadableSpeed = operation.downloadSpeedMeasure.humanReadableSpeed;
//        
//        NSTimeInterval remainingTimeInSeconds = [operation.downloadSpeedMeasure remainingTimeOfTotalSize:totalBytesExpectedToRead numberOfCompletedBytes:totalBytesRead];
//        NSString *humanReadableRemaingTime = [operation.downloadSpeedMeasure humanReadableRemainingTimeOfTotalSize:totalBytesExpectedToRead numberOfCompletedBytes:totalBytesRead];
//    }];
//}

- (void)mp_NEVPNStatusChanged:(StatusChanged)statusChanged
{
    self.observer = [[NSNotificationCenter defaultCenter] addObserverForName:NEVPNStatusDidChangeNotification
                                                      object:self.vpnManager.connection
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note)
    {
        statusChanged(self.vpnManager.connection.status);
    }];
}

- (void)mp_RemoveNEVPNStatusChangedNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self.observer name:NEVPNStatusDidChangeNotification object:self.vpnManager.connection];
}

@end



