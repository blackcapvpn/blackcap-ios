//
//  BlackCapVPNCommon.m
//  BlackCapVPNManager
//
//  Created by rstar on 2017/6/15.
//  Copyright © 2017. rstar. All rights reserved.
//

#import "BlackCapVPNCommon.h"

@implementation BlackCapVPNCommon
NSString * const kBlackCapVPNConfigKey = @"kBlackCapVPNConfigKey";

+ (NSUserDefaults *)sharedUserDefaults {
    return [[NSUserDefaults alloc] initWithSuiteName:BlackCapVPNAppGroupsName];
}

+ (BOOL)saveConfig:(id)config {
    NSUserDefaults *userDefaults = [self sharedUserDefaults];
    NSData * data  = [NSKeyedArchiver archivedDataWithRootObject:config];
    [userDefaults setObject:data forKey:kBlackCapVPNConfigKey];
    return [userDefaults synchronize];
}

+ (id)getConfig {
    NSUserDefaults *userDefaults = [self sharedUserDefaults];
    if ([userDefaults objectForKey:kBlackCapVPNConfigKey]) {
        NSData *data = [userDefaults objectForKey:kBlackCapVPNConfigKey];
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    return nil;
}


#pragma mark - KeyChain BEGIN

NSString * const BlackCapVPNPasswordIdentifier = @"BlackCapVPNPasswordIdentifier";
NSString * const BlackCapVPNSharePrivateKeyIdentifier = @"BlackCapVPNSharePrivateKeyIdentifier";

+ (NSString *)getServiceName {
//    return [[NSBundle mainBundle] bundleIdentifier];
    return @"BlackCapVPN.vpnconfig.service";
}

+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    
    [searchDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrAccount];
    [searchDictionary setObject:[self getServiceName] forKey:(__bridge id)kSecAttrService];
    
    return searchDictionary;
}

+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    
    [searchDictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    [searchDictionary setObject:@YES forKey:(__bridge id)kSecReturnPersistentRef];
    
    CFTypeRef result = NULL;
    SecItemCopyMatching((__bridge CFDictionaryRef)searchDictionary, &result);
    
    return (__bridge_transfer NSData *)result;
}

+ (BOOL)createKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier {
    NSMutableDictionary *dictionary = [self newSearchDictionary:identifier];
    
    OSStatus status = SecItemDelete((__bridge CFDictionaryRef)dictionary);
    
    NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:passwordData forKey:(__bridge id)kSecValueData];
    
    status = SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL);
    
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}
#pragma mark - KeyChain END


+ (void)insertLogIntoDataBaseWithLog:(NSString *)log
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    VpnLogItem *item = [[VpnLogItem alloc] init];
    item.log = log;
    item.time = [NSDate date];
    [realm addObject:item];
    [realm commitWriteTransaction];
}

+ (void)deleteDatabase {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteAllObjects];
    [realm commitWriteTransaction];
}

@end
