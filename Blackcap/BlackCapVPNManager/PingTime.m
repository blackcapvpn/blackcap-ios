//
//  PingTime.m
//  Blackcap
//
//  Created by rstar on 8/6/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "PingTime.h"

@implementation PingTime

- (PingTime*)init {
    self = [super init];
    if (self) {
        self.pingers = [[NSMutableDictionary alloc] init];
        self.delays = [[NSMutableDictionary alloc] init];
        self.pingTimers = [[NSMutableDictionary alloc] init];
        
        self.pingTimes = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (void)simplePing:(SimplePing *)pinger didFailWithError:(NSError *)error {
    [pinger stop];
}

- (void)simplePing:(SimplePing *)pinger didStartWithAddress:(NSData *)address {
    [pinger sendPingWithData:nil];
}

- (void)simplePing:(SimplePing *)pinger didReceiveUnexpectedPacket:(NSData *)packet {
}

- (void)simplePing:(SimplePing *)pinger didFailToSendPacket:(NSData *)packet sequenceNumber:(uint16_t)sequenceNumber error:(NSError *)error {
    [pinger stop];
}

- (void)simplePing:(SimplePing *)pinger didSendPacket:(NSData *)packet sequenceNumber:(uint16_t)sequenceNumber {
    [self.delays setValue:[NSDate date] forKey:pinger.hostName];
}

- (void)simplePing:(SimplePing *)pinger didReceivePingResponsePacket:(NSData *)packet sequenceNumber:(uint16_t)sequenceNumber {
    NSDate *oldDate = [self.delays objectForKey:pinger.hostName];
    NSString *delayTime = [NSString stringWithFormat:@"%i", (int)([oldDate timeIntervalSinceNow] * -1000.0)];
    
    [self.pingTimes setObject:delayTime forKey:pinger.hostName];
    [kUserDefaults setObject:self.pingTimes forKey:USERDEFAULTS_PING_TIMES];
}

- (void)createPinger:(NSString*)ipAddr {
    SimplePing *pinger = [[SimplePing alloc] initWithHostName:ipAddr];
    pinger.addressStyle = SimplePingAddressStyleICMPv4;
    pinger.delegate = self;
    [self.pingers setValue:pinger forKey:ipAddr];
    
    NSDate *delay = [NSDate date];
    [self.delays setValue:delay forKey:ipAddr];
    
    NSTimer *pingTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(pingTimerAction:) userInfo:ipAddr repeats:YES];
    [self.pingTimers setValue:pingTimer forKey:ipAddr];
    
    [pinger start];
}

- (void)pingTimerAction:(NSTimer *)timer {
    NSString *ipAddr = timer.userInfo;
    
    if ([self.pingTimers valueForKey:ipAddr]) {
        [[self.pingTimers valueForKey:ipAddr] invalidate];
        [self.pingTimers removeObjectForKey:ipAddr];
    }
    
    if ([self.pingers valueForKey:ipAddr]) {
        [[self.pingers valueForKey:ipAddr] stop];
    }
    
    [timer invalidate];
}
@end
