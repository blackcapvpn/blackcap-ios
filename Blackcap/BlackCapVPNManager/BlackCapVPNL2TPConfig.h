//
//  BlackCapVPNL2TPConfig.h
//  Blackcap
//
//  Created by rstar on 7/2/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "BlackCapVPNConfig.h"

@interface BlackCapVPNL2TPConfig : BlackCapVPNConfig

@property (nonatomic, copy, nullable) NSString *sharePrivateKey;

@end
