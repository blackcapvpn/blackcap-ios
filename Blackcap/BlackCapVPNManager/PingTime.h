//
//  PingTime.h
//  Blackcap
//
//  Created by rstar on 8/6/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PingTime : NSObject<SimplePingDelegate>

@property (nonatomic, strong) NSMutableDictionary *pingers;            //SimplePing Array
@property (nonatomic, strong) NSMutableDictionary *delays;             //NSDate Array
@property (nonatomic, strong) NSMutableDictionary *pingTimers;         //NSTimer Array

@property (nonatomic, strong) NSMutableArray *sentTimes;          //NSTimeInterval Array
@property (nonatomic, strong) NSMutableArray *receivedTimes;      //NSTimeInterval Array

@property (nonatomic, strong) NSMutableArray *vpnServers;         //BlackCapServer Array
@property (nonatomic, strong) BlackCapServer *curServer;
@property (nonatomic, strong) NSProxy *proxy;

@property (nonatomic, strong) NSMutableDictionary *pingTimes;

- (PingTime*)init;
- (void)createPinger:(NSString*)ipAddr;
@end
