//
//  BlackCapServer.h
//  Blackcap
//
//  Created by rstar on 6/27/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BlackCapServer : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *ip;
@property (nonatomic, strong) NSString *domainName;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property NSInteger weight;
@property NSInteger currentLoad;
@property NSInteger loadPercentage;
@property BOOL up;
@property BOOL supportsPPTP;
@property BOOL supportsSSTP;
@property BOOL supportsL2TP;
@property BOOL supportsOpenVPN;
@property BOOL supportsSoftEther;

@property (nonatomic, strong) NSString *pingTime;

- (BlackCapServer*)initWithDictionary:(id)dict;
@end
