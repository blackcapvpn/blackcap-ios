//
//  BlackCapVPNIKEv2Config.h
//  BlackCapVPNManager
//
//  Created by rstar on 2017/07/01.
//  Copyright © 2017. rstar. All rights reserved.
//

#import "BlackCapVPNIPSecConfig.h"

@interface BlackCapVPNIKEv2Config : BlackCapVPNIPSecConfig

@property (copy, nullable) NSString *serverCertificateIssuerCommonName;
@property (copy, nullable) NSString *serverCertificateCommonName;

@end
