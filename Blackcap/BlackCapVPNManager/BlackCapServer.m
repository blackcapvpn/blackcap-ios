//
//  BlackCapServer.m
//  Blackcap
//
//  Created by rstar on 6/27/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "BlackCapServer.h"

@implementation BlackCapServer

- (BlackCapServer*)initWithDictionary:(id)dict {
    self = [super init];
    if (self) {
        self.name = [dict objectForKey:@"name"];
        self.ip = [dict objectForKey:@"ip"];
        self.domainName = [dict objectForKey:@"domainName"];
        self.country = [dict objectForKey:@"country"];
        self.city = [dict objectForKey:@"city"];
        self.weight = [[dict objectForKey:@"weight"] integerValue];
        self.currentLoad = [[dict objectForKey:@"currentLoad"] integerValue];
        self.loadPercentage = [[dict objectForKey:@"loadPercentage"] integerValue];
        self.up = [[dict objectForKey:@"up"] boolValue];
        self.supportsPPTP = [[dict objectForKey:@"supportsPPTP"] boolValue];
        self.supportsSSTP = [[dict objectForKey:@"supportsSSTP"] boolValue];
        self.supportsL2TP = [[dict objectForKey:@"supportsL2TP"] boolValue];
        self.supportsOpenVPN = [[dict objectForKey:@"supportsOpenVPN"] boolValue];
        self.supportsSoftEther = [[dict objectForKey:@"supportsSoftEther"] boolValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.ip forKey:@"ip"];
    [encoder encodeObject:self.domainName forKey:@"domainName"];
    [encoder encodeObject:self.country forKey:@"country"];
    [encoder encodeObject:self.city forKey:@"city"];
    [encoder encodeInteger:self.weight forKey:@"weight"];
    [encoder encodeInteger:self.currentLoad forKey:@"currentLoad"];
    [encoder encodeInteger:self.loadPercentage forKey:@"loadPercentage"];
    [encoder encodeBool:self.up forKey:@"up"];
    [encoder encodeBool:self.supportsPPTP forKey:@"supportsPPTP"];
    [encoder encodeBool:self.supportsSSTP forKey:@"supportsSSTP"];
    [encoder encodeBool:self.supportsL2TP forKey:@"supportsL2TP"];
    [encoder encodeBool:self.supportsOpenVPN forKey:@"supportsOpenVPN"];
    [encoder encodeBool:self.supportsSoftEther forKey:@"supportsSoftEther"];
    
    [encoder encodeObject:self.pingTime forKey:@"pingTime"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        
        self.name = [decoder decodeObjectForKey:@"name"];
        self.ip = [decoder decodeObjectForKey:@"ip"];
        self.domainName = [decoder decodeObjectForKey:@"domainName"];
        self.country = [decoder decodeObjectForKey:@"country"];
        self.city = [decoder decodeObjectForKey:@"city"];
        self.weight = [decoder decodeIntegerForKey:@"weight"];
        self.currentLoad = [decoder decodeIntegerForKey:@"currentLoad"];
        self.loadPercentage = [decoder decodeIntegerForKey:@"loadPercentage"];
        self.up = [decoder decodeBoolForKey:@"up"];
        self.supportsPPTP = [decoder decodeBoolForKey:@"supportsPPTP"];
        self.supportsSSTP = [decoder decodeBoolForKey:@"supportsSSTP"];
        self.supportsL2TP = [decoder decodeBoolForKey:@"supportsL2TP"];
        self.supportsOpenVPN = [decoder decodeBoolForKey:@"supportsOpenVPN"];
        self.supportsSoftEther = [decoder decodeBoolForKey:@"supportsSoftEther"];
        
        self.pingTime = [decoder decodeObjectForKey:@"pingTime"];
    }
    return self;
}
@end
