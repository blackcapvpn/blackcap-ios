//
//  BlackCapVPNIPSecConfig.h
//  BlackCapVPNManager
//
//  Created by rstar on 2017/07/01.
//  Copyright © 2017. rstar. All rights reserved.
//

#import "BlackCapVPNConfig.h"

@interface BlackCapVPNIPSecConfig : BlackCapVPNConfig

@property (nonatomic, copy, nullable) NSString *sharePrivateKey;
@property (nonatomic, copy, nullable) NSString *localIdentifier;
@property (nonatomic, copy, nullable) NSString *remoteIdentifier;

@property (nonatomic, copy) NSData * _Nullable identityData;
@property (nonatomic, copy) NSString * _Nullable identityDataPassword;

@end
