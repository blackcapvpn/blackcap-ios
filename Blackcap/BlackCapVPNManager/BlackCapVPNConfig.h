//
//  BlackCapVPNConfig.h
//  BlackCapVPNManager
//
//  Created by rstar on 2017/07/01.
//  Copyright © 2017. rstar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BlackCapVPNConfig : NSObject
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;

@end
