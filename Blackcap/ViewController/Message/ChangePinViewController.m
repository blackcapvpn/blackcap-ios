//
//  ConfirmPinViewController.m
//  Blackcap
//
//  Created by JDev on 14/11/2017.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "ChangePinViewController.h"
#import "RegistrationViewController.h"
#import "SendExternalFileViewController.h"
#import "MyBlackcap-Swift.h"
#import "SignalsNavigationController.h"
#import "ViewControllerUtils.h"
#import <SignalServiceKit/TSAccountManager.h>
#import <SignalServiceKit/TSDatabaseView.h>
#import "Environment.h"

@interface ChangePinViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tfPin;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPin;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;

@end

@implementation ChangePinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - self methods
#pragma mark - Button Actoin methods
- (IBAction)btnNumberAction:(id)sender {
    NSString *pin = [self.tfPin text];
    
    if (pin.length == 4) {
        return;
    } else if (pin.length == 3) {
        [self.btnCheck setHidden:NO];
    }
    NSString *num = [((UIButton*)sender) currentTitle];
    [self.tfPin setText:[NSString stringWithFormat:@"%@%@", [self.tfPin text], num]];
}

- (IBAction)btnDeleteAction:(id)sender {
    NSString *pin = [self.tfPin text];
    if ([pin length] == 0) return;
    [self.tfPin setText:[pin substringToIndex:[pin length] - 1]];
    [self.btnCheck setHidden:YES];
    
}

- (IBAction)btnForgotPinAction:(id)sender {
}

- (IBAction)btnAcceptAction:(id)sender {
    NSString *pin = [self.tfPin text];
    [Environment.preferences setPinNumber:pin];
    [self showAlertView:APPNAME message:LocalizedStringFromBundle(kLocaleBundle, @"PIN_SET_SUCCESS_MESSAGE", @"") handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - Pin number forgot
- (void)forgotPin {
    [self.btnForgotPin setHidden:NO];
    [self.tfPin setHidden:YES];
}


@end
