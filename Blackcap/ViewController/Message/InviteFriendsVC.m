//
//  InviteFriendsVC.m
//  Blackcap
//
//  Created by Rstar Dev on 6/12/19.
//  Copyright © 2019 rstar. All rights reserved.
//

#import "InviteFriendsVC.h"

@interface InviteFriendsVC ()

@property (weak, nonatomic) IBOutlet UIButton *btnCopy;
@property (weak, nonatomic) IBOutlet UILabel *lblInviteContent;
@property (weak, nonatomic) IBOutlet UILabel *lblInviteUrl;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation InviteFriendsVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initView];
}

- (void)initView {
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self.btnCopy.layer setBorderWidth:2.0];
    [self.btnCopy.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.btnCopy setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.btnCopy setBackgroundColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.btnCopy setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnCopy setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    
    [self.btnCopy setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BC_INVITE_COPY", @"") forState:UIControlStateNormal];
    self.lblInviteContent.text = LocalizedStringFromBundle(kLocaleBundle, @"BC_INVITE_CONTENT", @"");
    self.lblInviteUrl.text = LocalizedStringFromBundle(kLocaleBundle, @"BC_INVITE_URL", @"");
    self.lblTitle.text = LocalizedStringFromBundle(kLocaleBundle, @"INVITE_FRIENDS_PICKER_TITLE", @"");

}

- (IBAction)btnCopyAction:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = LocalizedStringFromBundle(kLocaleBundle, @"BC_INVITE_URL", @"");
}

#pragma mark - Button Back Action
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

