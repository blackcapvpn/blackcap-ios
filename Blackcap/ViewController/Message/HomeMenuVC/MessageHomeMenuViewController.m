//
//  MenuViewController.m
//  Blackcap
//
//  Created by rstar on 6/22/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "MessageHomeMenuViewController.h"
#import "MessageHomeMenuCell.h"
#import "WebViewController.h"

@interface MessageHomeMenuViewController ()
{
    NSArray *dataArray;
}
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation MessageHomeMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
}

- (void)initView {
    dataArray = [[NSArray alloc] initWithObjects:
                 LocalizedStringFromBundle(kLocaleBundle, @"MESSAGE_HOME_MENU_NEW_GROUP", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"MESSAGE_HOME_MENU_MARK_ALL_READ", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"MESSAGE_HOME_MENU_INVITE_CONTACTS", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"MESSAGE_HOME_MENU_IMPORT_EXPORT", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"MESSAGE_HOME_MENU_SETTINGS", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"MESSAGE_HOME_MENU_HELP", @""),
                 nil];
    
    [self.menuTableView reloadData];
}

- (IBAction)bgButtonAction:(id)sender {
    //[self.navigationController popViewControllerAnimated:NO];
    [self dismissModalViewControllerAnimated:NO];
}

#pragma mark - tableview datasource and delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"MessageHomeMenuCell";
    MessageHomeMenuCell *cell=(MessageHomeMenuCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"MessageHomeMenuCell" owner:nil options:nil];
        cell=[nib objectAtIndex:0];
    }
    cell.m_label.text = [dataArray objectAtIndex:indexPath.row];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    cell.line_label.hidden = YES;
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:210.0/255.0 blue:200.0/255.0 alpha:0.3];
    [cell setSelectedBackgroundView:bgColorView];
    cell.m_label.highlightedTextColor = [UIColor whiteColor];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    NSLog(@"Row : %ld", (long)indexPath.row);
    
    if (indexPath.row == 0) // New group
    {
//        UINavigationController *basicNav = [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"BasicViewController"]];
//        [basicNav setNavigationBarHidden:YES];
    }
    else if (indexPath.row == 1) // Mark all read
    {
    }
    else if (indexPath.row == 2) // Invite contacts
    {
    }
    else if (indexPath.row == 3) // Import/export
    {
    }
    else if (indexPath.row == 4) // Settings
    {
    }
    else if (indexPath.row == 5) // Help
    {
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.backgroundColor = (__bridge CGColorRef _Nullable)([UIColor clearColor]);//optional
    cell.backgroundView = nil;
}

@end


