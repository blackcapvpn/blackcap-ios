//
//  ConfirmPinViewController.m
//  Blackcap
//
//  Created by JDev on 14/11/2017.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "ConfirmPinViewController.h"
#import "RegistrationViewController.h"
#import "SendExternalFileViewController.h"
#import "MyBlackcap-Swift.h"
#import "SignalsNavigationController.h"
#import "ViewControllerUtils.h"
#import <SignalServiceKit/TSAccountManager.h>
#import <SignalServiceKit/TSDatabaseView.h>
#import <SignalServiceKit/TSStorageManager.h>
#import "Environment.h"

@interface ConfirmPinViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tfPin;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPin;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;

@property BOOL isChecking;
@property int numIncorrect;
@property BOOL isRest;
@end

@implementation ConfirmPinViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self customSetup];
    
    self.isChecking = NO;
    self.numIncorrect = 0;
    self.isRest = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - self methods
- (void)textFieldDidChange :(UITextField*) textField{
    if (!self.isRest) {
        NSString *pin = [textField text];
        if (pin.length < 4) return;
        
        if (pin.length == 4) {
            self.isChecking = true;
            
            if ([self checkPinValidate]) {
                [self openSignalViewController];
            }
            
            self.isChecking = false;
        }
    }
}

#pragma Sidemenu Button Action
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark - Button Actoin methods
- (IBAction)btnNumberAction:(id)sender {
    if (self.isRest) {
        NSString *pin = [self.tfPin text];
        
        if (pin.length == 4) {
            return;
        } else if (pin.length == 3) {
            [self.btnCheck setHidden:NO];
        }
        NSString *num = [((UIButton*)sender) currentTitle];
        [self.tfPin setText:[NSString stringWithFormat:@"%@%@", [self.tfPin text], num]];
    } else {
        if (self.isChecking) return;
        NSString *num = [((UIButton*)sender) currentTitle];
        [self.tfPin setText:[NSString stringWithFormat:@"%@%@", [self.tfPin text], num]];
        
        [self textFieldDidChange:self.tfPin];
    }
}

- (IBAction)btnDeleteAction:(id)sender {
    if (self.isRest) {
        NSString *pin = [self.tfPin text];
        if ([pin length] == 0) return;
        [self.tfPin setText:[pin substringToIndex:[pin length] - 1]];
        [self.btnCheck setHidden:YES];
    } else {
        NSString *pin = [self.tfPin text];
        if ([pin length] == 0 || self.isChecking) return;
        [self.tfPin setText:[pin substringToIndex:[pin length] - 1]];
        
        [self textFieldDidChange:self.tfPin];
    }
}

- (IBAction)btnForgotPinAction:(id)sender {
    self.isRest = YES;
    [self.tfPin setText:@""];
    [self.tfPin setHidden:NO];
    [self.btnForgotPin setHidden:YES];
    
    [self.lblTitle setText:LocalizedStringFromBundle(kLocaleBundle, @"CONFIRM_PIN_RESET", @"")];
}

- (IBAction)btnAcceptAction:(id)sender {
    NSString *pin = [self.tfPin text];
    [Environment.preferences setPinNumber:pin];
    [self showAlertView:APPNAME message:LocalizedStringFromBundle(kLocaleBundle, @"PIN_SET_SUCCESS_MESSAGE", @"") handler:^(UIAlertAction * _Nonnull action) {
        [self openSignalViewController];
    }];
}

#pragma mark - Check pin number is valid
- (bool)checkPinValidate {
    if ([self.tfPin.text isEqualToString:[Environment.preferences pinNumber]]) return YES;
    
    [self.tfPin setText:@""];
    self.numIncorrect++;
    if (self.numIncorrect == 3) [self forgotPin];
    return NO;
}

#pragma mark - Pin number forgot
- (void)forgotPin {
    [self showAlertView:APPNAME message:LocalizedStringFromBundle(kLocaleBundle, @"INPUT_PIN_THREE_TIME_MESSAGE", @"Type wrong PIN 3 times") handler:^(UIAlertAction * _Nonnull action) {
        [[TSStorageManager sharedManager] deleteThreadsAndMessages];
        [self.btnForgotPin setHidden:NO];
        [self.tfPin setHidden:YES];
    }];
}

// Open signal initial view controller
- (void)openSignalViewController
{
//    if ([TSDatabaseView hasPendingViewRegistrations]) {
//        return;
//    }

    SWRevealViewController *revealController = self.revealViewController;
    if ([TSAccountManager isRegistered]) {
        HomeViewController *homeView = [HomeViewController new];
        SignalsNavigationController *navigationController = [[SignalsNavigationController alloc] initWithRootViewController:homeView];
        [revealController pushFrontViewController:navigationController animated:YES];
//        [[kSharedAppdelegate window] setRootViewController:navigationController];
    } else {
        RegistrationViewController *viewController = [RegistrationViewController new];
        OWSNavigationController *navigationController = [[OWSNavigationController alloc] initWithRootViewController:viewController];
        navigationController.navigationBarHidden = YES;
        [revealController pushFrontViewController:navigationController animated:YES];
//        [[kSharedAppdelegate window] setRootViewController:navigationController];
    }
    
}


@end
