//
//  SettingsViewController.m
//  Blackcap
//
//  Created by rstar on 6/22/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "ConversationSettingsViewController.h"
#import "SettingsCell.h"
#import "WebViewController.h"

@interface ConversationSettingsViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSArray *titleArray, *dataArray,*checkArray;
}
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnErase;

@end

@implementation ConversationSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initView];
}

- (void)initView {
    [self.navigationController setNavigationBarHidden:YES animated:NO];

    // Login button theme when clicking
    [self.btnErase.layer setBorderWidth:2.0];
    [self.btnErase.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.btnErase setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.btnErase setBackgroundColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.btnErase setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnErase setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];

    
    titleArray = [[NSArray alloc] initWithObjects:
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_CHAT_SETTING_SECTION_TITLE", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_PRIVACY", @""),
                  nil];
    
    dataArray = [[NSArray alloc] initWithObjects:
                 [[NSArray alloc] initWithObjects:
                  LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_MUTE_CONVERSATION", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_NOTIFICATION_SOUND", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_VIBRATE", @""),
                  nil],
                 [[NSArray alloc] initWithObjects:
                  LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_VIEW_SAFETY_CODE", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_BLOCK_CONTACT", @""),
                  nil],
                 nil];
    
    checkArray = [[NSArray alloc] initWithObjects:
                  [[NSArray alloc] initWithObjects:@"yes", @"yes", @"yes", nil],
                  [[NSArray alloc] initWithObjects:@"no", @"no", nil],
                  nil];
    
    [self.settingsTableView reloadData];
}

#pragma mark - Button Back Action
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnEraseAction:(id)sender {
}

#pragma mark - tableview datasource and delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dataArray objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 45)];
    
    UILabel *lblHeaderTitle = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, kDeviceWidth - 45, 20)];
    [lblHeaderTitle setFont:SystemBoldFont(14)];
    [lblHeaderTitle setTextColor:COLOR(0)];
    [viewHeader addSubview:lblHeaderTitle];
    [viewHeader setBackgroundColor:COLOR(255)];
    
    [lblHeaderTitle setText:[titleArray objectAtIndex:section]];
    
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 20)];
    
    UILabel *lblFooter = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, kDeviceWidth - 30, 1)];
    [viewFooter addSubview:lblFooter];
    [viewFooter setBackgroundColor:COLOR(255)];

    if (section != [dataArray count] - 1) {
        [lblFooter setBackgroundColor:COLOR(120)];
    }
    return viewFooter;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"SettingsCell";
    SettingsCell *cell=(SettingsCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"SettingsCell" owner:nil options:nil];
        cell=[nib objectAtIndex:0];
    }
    cell.m_label.text = [[dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if ([[[checkArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"yes"]) {
        cell.checkBox.hidden = NO;
        if ([kUserDefaults objectForKey:cell.m_label.text]) {
            [cell.checkBox setOn:[kUserDefaults boolForKey:cell.m_label.text]];
        }
    } else {
        cell.checkBox.hidden = YES;
        cell.btnCell.hidden = YES;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:210.0/255.0 blue:200.0/255.0 alpha:0.3];
    [cell setSelectedBackgroundView:bgColorView];
    
    [cell.btnCell addTarget:cell action:@selector(btnCellClicked:) forControlEvents:UIControlEventTouchDown];
    //cell.m_label.highlightedTextColor = [UIColor whiteColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    NSString *selectedStr = [[dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_MUTE_CONVERSATION", @"")])
    {
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_NOTIFICATION_SOUND", @"")])
    {
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_VIBRATE", @"")])
    {
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_VIEW_SAFETY_CODE", @"")])
    {
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"CONVERSATION_SETTINGS_BLOCK_CONTACT", @"")])
    {
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.backgroundColor = (__bridge CGColorRef _Nullable)([UIColor clearColor]);//optional
    cell.backgroundView = nil;
}

@end


