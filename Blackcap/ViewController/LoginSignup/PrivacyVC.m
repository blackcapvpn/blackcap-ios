//
//  PrivacyVC.m
//  Blackcap
//
//  Created by Rstar Dev on 5/23/19.
//  Copyright © 2019 rstar. All rights reserved.
//

#import "PrivacyVC.h"

@interface PrivacyVC ()

@property (weak, nonatomic) IBOutlet UILabel *lblPrivacyTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivacyHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivacy1;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivacy2;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivacy3;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivacy4;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivacy5;
@property (weak, nonatomic) IBOutlet UILabel *lblReadMore;
@property (weak, nonatomic) IBOutlet UILabel *lblSeeOur;

@property (weak, nonatomic) IBOutlet UIButton *btnPrivacy;
@property (weak, nonatomic) IBOutlet UIButton *btnOpenApp;
@property (weak, nonatomic) IBOutlet UIButton *btnChangeLanguage;


@end

@implementation PrivacyVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initView];
}

- (void)initView {
    self.lblPrivacyTitle.text = LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_TITLE", @"");
    self.lblPrivacyHeader.text = LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_HEADER", @"");
    self.lblPrivacy1.text = LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_LABEL1", @"");
    self.lblPrivacy2.text = LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_LABEL2", @"");
    self.lblPrivacy3.text = LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_LABEL3", @"");
    self.lblPrivacy4.text = LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_LABEL4", @"");
    self.lblPrivacy5.text = LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_LABEL5", @"");
    self.lblReadMore.text = LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_READ_MORE", @"");
    self.lblSeeOur.text = LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_SEE_OUR", @"");
    [self.btnPrivacy setTitle:LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_PRIVACY_POLICY", @"") forState:UIControlStateNormal];
    [self.btnOpenApp setTitle:LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_OPEN_APP", @"") forState:UIControlStateNormal];
    [self.btnChangeLanguage setTitle:LocalizedStringFromBundle(kLocaleBundle, @"PRIVACY_CHANGE_LANGUAGE", @"") forState:UIControlStateNormal];

}

- (IBAction)OpenAppAction:(id)sender {
    [kUserDefaults setBool:YES forKey:@"AgreePrivacyPolicy"];
    [self.navigationController pushViewController:[ChooseViewController loadViewControllerWithId:@"LoginViewController"] animated:YES];
}

- (IBAction)privacyPolicyAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://blackcap.zone/privacy.php"]];
}

- (IBAction)changeLanguageAction:(id)sender {
    if ([[kUserDefaults objectForKey:USERDEFAULTS_LANGUAGE] isEqualToString:@"English"]) {
        [kUserDefaults setObject:@"Español" forKey:USERDEFAULTS_LANGUAGE];
        [self changeLocale:@"es"];
    } else {
        [kUserDefaults setObject:@"English" forKey:USERDEFAULTS_LANGUAGE];
        [self changeLocale:@"en"];
    }
    [kUserDefaults synchronize];
}

- (void)changeLocale:(NSString*)locale {
    NSString *path = [[NSBundle mainBundle] pathForResource:locale ofType:@"lproj"];
    NSBundle *bundle = [NSBundle mainBundle];
    if (path) {
        bundle = [NSBundle bundleWithPath:path];
    }
    
    [kSharedAppdelegate setLocaleBundle:bundle];
    
    [self initView];
}

@end
