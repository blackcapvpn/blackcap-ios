//
//  WebViewController.h
//  Blackcap
//
//  Created by rstar on 6/29/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *webViewUrl;
@property (nonatomic, strong) NSString *titleStr;
@end
