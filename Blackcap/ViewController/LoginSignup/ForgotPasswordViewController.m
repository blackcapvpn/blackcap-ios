//
//  ForgotPasswordViewController.m
//  Blackcap
//
//  Created by rstar on 6/19/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController () <UITextFieldDelegate>
{
    NSString *email;
}
@property (nonatomic, readwrite) CGFloat keyboardShowHeight;
@property (nonatomic, readwrite) BOOL keyboardIsShowing;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfBottomConstraint;

- (IBAction)btnSubmitAction:(id)sender;
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self unregisterKeyboardNotifications];
}


#pragma mark - self methods
- (void)initView {
    // tapgesture for textfield keyboard disappearing
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTap:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.contentView addGestureRecognizer:tapGesture];
    
    // Login button theme when clicking
    [self.btnSubmit.layer setBorderWidth:2.0];
    [self.btnSubmit.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.btnSubmit setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.btnSubmit setBackgroundColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.btnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnSubmit setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}

- (BOOL)isValidInput {
    if ([self.tfEmail.text length]) {
        if ([MyUtils validateEmailWithString:self.tfEmail.text]) {
            email = self.tfEmail.text;
        } else {
            [self showAlertView:APPNAME message:LocalizedStringFromBundle(kLocaleBundle, @"INVALID_EMAIL", @"") handler:^(UIAlertAction * _Nonnull action) {
                [self.tfEmail becomeFirstResponder];
            }];
            return NO;
        }
    } else {
        //[self showAlertView:APPNAME message:@"Input email address!" handler:^(UIAlertAction * _Nonnull action) {
            [self.tfEmail becomeFirstResponder];
        //}];
        return NO;
    }
    return YES;
}

#pragma mark - Event handling method
- (void)contentViewTap:(UITapGestureRecognizer *)recognizer
{
    [self.contentView endEditing:YES];
}

#pragma mark - Button Actions
- (IBAction)btnSubmitAction:(id)sender {
    if ([self isValidInput]) {
        [self.contentView endEditing:YES];
    }
}
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self isValidInput]) {
        [self btnSubmitAction:nil];
    }
    return YES;
}

#pragma mark - Keyboard
- (void)registerKeyboardNotifications {
    // Register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)unregisterKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

/**
 *  Handler method for UIKeyBoardWillShowNotification. Performs UI changes when keyboard appears
 *
 *  @param notification UIKeyBoardWillShowNotification object
 */

-(void) keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardFrame = [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardFrame.size.height;
    if (keyboardFrame.origin.y == 0) {//landscape
        keyboardHeight = keyboardFrame.size.width;
    }
    if (keyboardHeight != self.keyboardShowHeight)
        self.keyboardIsShowing = NO;
    
    CGFloat keyboardShowHeight = keyboardHeight;
    keyboardHeight -= self.keyboardShowHeight;
    
    if (self.keyboardIsShowing == NO) {
        //        self.navigationItem.rightBarButtonItem = self.cancelButtonItem;
        
        self.keyboardIsShowing = YES;
        self.keyboardShowHeight = keyboardShowHeight;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:[[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
        [UIView setAnimationCurve:[[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]];
        
        if (self.tfBottomConstraint) {
            self.tfBottomConstraint.constant += keyboardHeight;
            [self.view layoutIfNeeded];
        }
        [UIView commitAnimations];
    }
}

/**
 *  Handler method for UIKeyBoardWillHideNotification. Perform UI changes when keyboard disappears.
 *
 *  @param notification UIKeyBoardWillHideNotification object
 */

- (void)keyboardWillHide:(NSNotification*)notification {
    if (!self.keyboardIsShowing) {
        return;
    }
    
    //    self.navigationItem.rightBarButtonItem = nil;
    
    self.keyboardIsShowing = NO;
    
    CGFloat keyboardHeight = self.keyboardShowHeight;//[[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]];
    
    if (self.tfBottomConstraint) {
        self.tfBottomConstraint.constant -= keyboardHeight;
        [self.view layoutIfNeeded];
    }
    
    [UIView commitAnimations];
    self.keyboardShowHeight = 0;
}


@end
