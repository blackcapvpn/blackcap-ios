//
//  WebViewController.m
//  Blackcap
//
//  Created by rstar on 6/29/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()<UIWebViewDelegate>
{
    MBProgressHUD *hud;
}
@property (weak, nonatomic) IBOutlet UIWebView *contentWebView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.lblTitle setText:self.titleStr];
    [self loadWebView:self.webViewUrl];
}

- (void)loadWebView:(NSString*)subUrl {
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", BASE_URL, subUrl];
    NSURL *nsUrl = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    
    [self.contentWebView loadRequest:request];
    
}


#pragma Sidemenu Button Action
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIWebView Delegate methods
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error : %@",error);
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"Info : %@",@"WebView loading started.");
    hud = [MBProgressHUD showHUDAddedTo:webView animated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"Info : %@",@"WebView loading finished.");
    if (hud) {
        [hud hideAnimated:YES];
    }
}

@end
