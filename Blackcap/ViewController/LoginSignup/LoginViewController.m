//
//  LoginViewController.m
//  Blackcap
//
//  Created by rstar on 6/19/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "LoginViewController.h"
#import "WebViewController.h"

typedef enum {
    Login_Success = 200,
    Login_IncorrectDetails = 403,
    Login_NoSubscription = 402
} LoginResponse;

@interface LoginViewController () <UITextFieldDelegate>
{
    NSString *username;
    NSString *password;
}
@property (nonatomic, readwrite) CGFloat keyboardShowHeight;
@property (nonatomic, readwrite) BOOL keyboardIsShowing;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginFieldBottomConstraint;
@property (weak, nonatomic) IBOutlet UITextField *tfUsername;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnForgot;

- (IBAction)btnLoginAction:(id)sender;
- (IBAction)btnForgotAction:(id)sender;
@end

@implementation LoginViewController

#pragma mark - Life Cycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];

    if ([self checkLoginStatus]) {
        if ([kUserDefaults objectForKey:USERDEFAULTS_TODAY_URL]) {
            [self moveMainPage:NO];
        } else {
            [self moveMainPage:YES];
        }
    } else {
        [self initView];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self unregisterKeyboardNotifications];
}

#pragma mark - Self functions
- (void)initView {
//    self.tfUsername.text = @"test@test.com";
//    self.tfPassword.text = @"test123";
    self.tfUsername.text = @"";
    self.tfPassword.text = @"";

    // tapgesture for textfield keyboard disappearing
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTap:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.contentView addGestureRecognizer:tapGesture];
    
    // Login button theme when clicking
    [self.btnLogin.layer setBorderWidth:2.0];
    [self.btnLogin.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.btnLogin setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.btnLogin setBackgroundColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.btnLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnLogin setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    
    [self.tfUsername setPlaceholder:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_USERNAME", @"")];
    [self.tfPassword setPlaceholder:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_PASSWORD", @"")];
    [self.btnLogin setTitle:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_LOGIN", @"") forState:UIControlStateNormal];
    [self.btnForgot setTitle:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_FORGOT_PASSWORD", @"") forState:UIControlStateNormal];

}

- (BOOL)checkLoginStatus {
    if ([kUserDefaults objectForKey:USERDEFAULTS_USERNAME] != nil && [kUserDefaults objectForKey:USERDEFAULTS_PASSWORD] != nil) {
        return YES;
    }
    return NO;
}

- (BOOL)isValidInput {
    if ([self.tfUsername.text length]) {
        username = self.tfUsername.text;
    } else {
        [self.tfUsername becomeFirstResponder];
//        [self showAlertView:@"Error" message:@"Input username!" handler:^(UIAlertAction * _Nonnull action) {
//        }];
        return NO;
    }
    if ([self.tfPassword.text length]) {
        password = self.tfPassword.text;
    } else {
        [self.tfPassword becomeFirstResponder];
//        [self showAlertView:@"Error" message:@"Input password!" handler:^(UIAlertAction * _Nonnull action) {
//        }];
        return NO;
    }
    return YES;
}

- (void)moveMainPage:(BOOL)isBasic
{
    UINavigationController *frontNav = isBasic ? [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"BasicViewController"]] : [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"AdvancedViewController"]];
    [frontNav setNavigationBarHidden:YES];
    UIViewController *menuVC = [ChooseViewController loadViewControllerWithId:@"MenuViewController"];
    SWRevealViewController *controller = [[SWRevealViewController alloc] initWithRearViewController:menuVC frontViewController:frontNav];
    controller.frontViewPosition = FrontViewPositionLeft;
    controller.rearViewRevealWidth = kDeviceWidth * 5 / 6;
    controller.rearViewRevealOverdraw = 40.0f;
    controller.rearViewRevealDisplacement = 40.0f;
    controller.rightViewRevealWidth = 260.0f;
    controller.rightViewRevealOverdraw = 60.0f;
    controller.rightViewRevealDisplacement = 40.0f;
    controller.bounceBackOnOverdraw = YES;
    controller.bounceBackOnLeftOverdraw = YES;
    controller.stableDragOnOverdraw = NO;
    controller.stableDragOnLeftOverdraw = NO;
    controller.presentFrontViewHierarchically = NO;
    controller.quickFlickVelocity = 250.0f;
    controller.toggleAnimationDuration = 0.3;
    controller.toggleAnimationType = SWRevealToggleAnimationTypeSpring;
    controller.springDampingRatio = 1;
    controller.replaceViewAnimationDuration = 0.25;
    controller.frontViewShadowRadius = 2.5f;
    controller.frontViewShadowOffset = CGSizeMake(0.0f, 20.0f);
    controller.frontViewShadowOpacity = 1.0f;
    controller.frontViewShadowColor = [UIColor blackColor];
    controller.draggableBorderWidth = 0.0f;
    controller.clipsViewsToBounds = NO;
    controller.extendsPointInsideHit = NO;
    ((AppDelegate*)kSharedAppdelegate).MainVC = controller;
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Event handling method
- (void)contentViewTap:(UITapGestureRecognizer *)recognizer
{
    [self.contentView endEditing:YES];
}

#pragma mark - Button Action
- (IBAction)btnLoginAction:(id)sender {
    if ([self isValidInput]) {
        [self.contentView endEditing:YES];
        
//        [kUserDefaults setObject:username forKey:USERDEFAULTS_USERNAME];
//        [kUserDefaults setObject:password forKey:USERDEFAULTS_PASSWORD];
//        [self moveMainPage:YES];
        
        [self callLogin];
    }
}

- (IBAction)btnForgotAction:(id)sender {
    [self.contentView endEditing:YES];
    //[self.navigationController pushViewController:[ChooseViewController loadViewControllerWithId:@"ForgotPasswordViewController"] animated:YES];
    
    WebViewController *webVC = [ChooseViewController loadViewControllerWithId:@"WebViewController"];
    webVC.titleStr = LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_RESET_PASSWORD", @"");
    webVC.webViewUrl = @"members/pwreset.php";
    [self.navigationController pushViewController:webVC animated:YES];
}

#pragma mark - TextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self isValidInput]) {
        [self btnLoginAction:nil];
    }
    return YES;
}

#pragma mark - Keyboard
- (void)registerKeyboardNotifications {
    // Register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)unregisterKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


/**
 *  Handler method for UIKeyBoardWillShowNotification. Performs UI changes when keyboard appears
 *
 *  @param notification UIKeyBoardWillShowNotification object
 */

-(void) keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardFrame = [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardFrame.size.height;
    if (keyboardFrame.origin.y == 0) {//landscape
        keyboardHeight = keyboardFrame.size.width;
    }
    if (keyboardHeight != self.keyboardShowHeight)
        self.keyboardIsShowing = NO;
    
    CGFloat keyboardShowHeight = keyboardHeight;
    keyboardHeight -= self.keyboardShowHeight;
    
    if (self.keyboardIsShowing == NO) {
        //        self.navigationItem.rightBarButtonItem = self.cancelButtonItem;
        
        self.keyboardIsShowing = YES;
        self.keyboardShowHeight = keyboardShowHeight;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:[[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
        [UIView setAnimationCurve:[[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]];
        
        if (self.loginFieldBottomConstraint) {
            if (keyboardHeight > 50) {
                self.loginFieldBottomConstraint.constant += (keyboardHeight - 50);
            } else {
                self.loginFieldBottomConstraint.constant += keyboardHeight;
            }
            [self.view layoutIfNeeded];
        }
        [UIView commitAnimations];
    }
}


/**
 *  Handler method for UIKeyBoardWillHideNotification. Perform UI changes when keyboard disappears.
 *
 *  @param notification UIKeyBoardWillHideNotification object
 */

- (void)keyboardWillHide:(NSNotification*)notification {
    if (!self.keyboardIsShowing) {
        return;
    }
    
    //    self.navigationItem.rightBarButtonItem = nil;
    
    self.keyboardIsShowing = NO;
    
    CGFloat keyboardHeight = self.keyboardShowHeight;//[[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]];
    
    if (self.loginFieldBottomConstraint) {
        self.loginFieldBottomConstraint.constant -= (keyboardHeight-50);
        [self.view layoutIfNeeded];
    }
    
    [UIView commitAnimations];
    self.keyboardShowHeight = 0;
}

#pragma mark - Call APIs
- (void)callLogin {
    NSDictionary *dict = @{@"u":username, @"p":password};
    [ApiManager postDataToURL:SUB_URL_LOGIN withParameter:dict authToken:nil sender:self responseHandler:^(id  _Nullable responseObject, bool isSuccess) {
        LoginResponse responseCode = [[[responseObject objectForKey:@"meta"] objectForKey:@"code"] intValue];
        if (responseCode == Login_Success) {
            [BlackCapVPNCommon insertLogIntoDataBaseWithLog:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_FINISHED", @"")];
            [kUserDefaults setObject:username forKey:USERDEFAULTS_USERNAME];
            [kUserDefaults setObject:password forKey:USERDEFAULTS_PASSWORD];
            [kUserDefaults setBool:[[[responseObject objectForKey:@"data"] objectForKey:@"vpn"] boolValue] forKey:USERDEFAULTS_LOGIN_STATUS_VPN];
            Boolean bMsg = [[[responseObject objectForKey:@"data"] objectForKey:@"messenger"] boolValue];
            [kUserDefaults setBool:bMsg forKey:USERDEFAULTS_LOGIN_STATUS_MESSENGER];
            int msgCount = bMsg ? 0 : [[[responseObject objectForKey:@"data"] objectForKey:@"message_count"] intValue];
            [kUserDefaults setInteger:msgCount forKey:USERDEFAULTS_FREE_MESSAGE_NUM];

            [self moveMainPage:YES];
        } else if (responseCode == Login_NoSubscription) {
            [BlackCapVPNCommon insertLogIntoDataBaseWithLog:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_FAILED", @"")];
            [self showAlertView:APPNAME
                        message:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_ERROR_NO_SUBSCRIPTION", @"")
                         button:LocalizedStringFromBundle(kLocaleBundle, @"LOG_VIEW_CLOSE", @"")
                        handler:nil];
        } else if (responseCode == Login_IncorrectDetails) {
            [BlackCapVPNCommon insertLogIntoDataBaseWithLog:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_FAILED", @"")];
            [self showAlertView:APPNAME
                        message:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_INCORRECT_DETAILS", @"")
                         button:LocalizedStringFromBundle(kLocaleBundle, @"LOG_VIEW_CLOSE", @"")
                        handler:nil];
        } else {
            [BlackCapVPNCommon insertLogIntoDataBaseWithLog:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_FAILED", @"")];
            [self showAlertView:APPNAME
                        message:LocalizedStringFromBundle(kLocaleBundle, @"LOGIN_ERROR_GENERAL", @"")
                         button:LocalizedStringFromBundle(kLocaleBundle, @"LOG_VIEW_CLOSE", @"")
                        handler:nil];
        }
    }];
}

@end
