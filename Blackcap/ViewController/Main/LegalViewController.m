//
//  LegalViewController.m
//  Blackcap
//
//  Created by JDev on 3/7/2018.
//  Copyright © 2018 rstar. All rights reserved.
//

#import "LegalViewController.h"

@interface LegalViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@end

@implementation LegalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
}

- (void)initView {
    self.lblTitle.text = LocalizedStringFromBundle(kLocaleBundle, @"LEGAL_NOTICE", @"");
    self.lblContent.text = LocalizedStringFromBundle(kLocaleBundle, @"LEGAL_NOTICE_CONTENT", @"");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
