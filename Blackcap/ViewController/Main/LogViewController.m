//
//  LogViewController.m
//  Blackcap
//
//  Created by rstar on 6/25/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "LogViewController.h"
#import "LogCell.h"
#import "WebViewController.h"


@interface LogViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    RLMResults *logItemArray;
    VpnLogItem *selectedLogItem;
}

@property (weak, nonatomic) IBOutlet UIButton *btnCopy;
@property (weak, nonatomic) IBOutlet UIButton *btnSupport;
@property (weak, nonatomic) IBOutlet UITableView *tvLogHistory;

//@property (nonatomic, strong) NSMutableArray *arrayLog;

@end

@implementation LogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initView];
}

#pragma mark - Self functions
- (void)initView {
//    [BlackCapVPNCommon deleteDatabase];
//    [BlackCapVPNCommon insertLogIntoDataBaseWithLog:@"login finished with result success"];
//    [BlackCapVPNCommon insertLogIntoDataBaseWithLog:@"Checking for updates"];
//    [BlackCapVPNCommon insertLogIntoDataBaseWithLog:@"login finished with result success"];
//    [BlackCapVPNCommon insertLogIntoDataBaseWithLog:@"login Got latest version is 1.0.0.0"];
    
    logItemArray = [VpnLogItem allObjects];
    [self.tvLogHistory reloadData];
    
    
    // Copy & Support button theme when clicking
    [self.btnCopy.layer setBorderWidth:2.0];
    [self.btnCopy.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.btnCopy setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.btnCopy setBackgroundColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnCopy setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnCopy setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    
    [self.btnSupport.layer setBorderWidth:2.0];
    [self.btnSupport.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.btnSupport setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.btnSupport setBackgroundColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnSupport setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnSupport setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];

}

#pragma mark - Button Back Action
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCopyAction:(id)sender {
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    
    NSString *copyStr = @"";
    for (VpnLogItem *item in logItemArray) {
        copyStr = [copyStr stringByAppendingString:[NSDate getLongDateStringFromDate:item.time]];
        copyStr = [copyStr stringByAppendingString:@" "];
        copyStr = [copyStr stringByAppendingString:item.log];
        copyStr = [copyStr stringByAppendingString:@"\n"];
    }
    
    if ([copyStr length] > 0) {
        [pb setString:copyStr];
    }
}

- (IBAction)btnSupportAction:(id)sender {
    WebViewController *webVC = [ChooseViewController loadViewControllerWithId:@"WebViewController"];
    webVC.titleStr = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SUPPORT", @"");
    webVC.webViewUrl = @"members/supporttickets.php";
    [self.navigationController pushViewController:webVC animated:YES];
}

#pragma mark - tableview datasource and delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [logItemArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.estimatedRowHeight = 40;
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"LogCell";
    LogCell *cell=(LogCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"LogCell" owner:nil options:nil];
        cell=[nib objectAtIndex:0];
    }
    VpnLogItem *item = [logItemArray objectAtIndex:indexPath.row];
    cell.lblLog.text = [NSString stringWithFormat:@"%@ %@", [NSDate getLongDateStringFromDate:item.time], item.log];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:210.0/255.0 blue:200.0/255.0 alpha:0.3];
    [cell setSelectedBackgroundView:bgColorView];
    //cell.m_label.highlightedTextColor = [UIColor whiteColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.backgroundColor = (__bridge CGColorRef _Nullable)([UIColor clearColor]);//optional
    cell.backgroundView = nil;
}


#pragma mark - Call APIs
- (void)callLogHistory {
    [ApiManager getDataFromURL:@"" withParameter:nil authToken:nil sender:self responseHandler:^(id  _Nullable responseObject, bool isSuccess) {
        if (responseObject) {
            [self.tvLogHistory reloadData];
        } else {
            [self showAlertView:APPNAME message:LocalizedStringFromBundle(kLocaleBundle, @"LOG_VIEW_NO_HISTORY", @"") button:LocalizedStringFromBundle(kLocaleBundle, @"LOG_VIEW_CLOSE", @"") handler:nil];
        }
    }];
}

@end
