//
//  AdvancedViewController.m
//  Blackcap
//
//  Created by rstar on 6/22/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "AdvancedViewController.h"
#import "PurchaseAlertVC.h"
#import "MyBlackcap-Swift.h"
#import <Crashlytics/Crashlytics.h>

@interface AdvancedViewController ()
{
    NSMutableArray *serverNameArray;
    BlackCapServer *currentConfiguredServer;
    BlackCapServer *selectedServer;
    BOOL bChangeServer;
}

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnConnect;
@property (weak, nonatomic) IBOutlet UIButton *btnDisconnect;
@property (weak, nonatomic) IBOutlet UITextField *tfLocation;
@property (weak, nonatomic) IBOutlet UITextField *tfIPAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnServerList;
@property (weak, nonatomic) IBOutlet UILabel *anchorLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnDrop;
@property (weak, nonatomic) IBOutlet UIButton *btnAutomatic;

@property (weak, nonatomic) IBOutlet UILabel *lblSelectServer;
@property (weak, nonatomic) IBOutlet UILabel *lblConnection;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblIpAddress;

@property (nonatomic, strong) DropDown *ddServer;
@property (nonatomic, strong) NSMutableArray *serverList;
@property (nonatomic, strong) BlackCapVPNManager *vpnManager;

@end

@implementation AdvancedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
    [self initViewStrings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[kSharedAppdelegate vpnManager] loadOpenVPNTunnelProviderManager:^(BOOL success, NSString *returnInfo) {
        if (success) {
            [[kSharedAppdelegate vpnManager] mp_NEVPNStatusChanged:^(enum NEVPNStatus status) {
                [self changeButtonsForState:status];
            }];
            [self changeButtonsForState:[[kSharedAppdelegate vpnManager] status]];
            [self handleTodayWidgetURL];
        } else {
            NSLog(@"%@", returnInfo);
        }
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[kSharedAppdelegate vpnManager] mp_RemoveNEVPNStatusChangedNotification];
}

#pragma mark - self methods
- (void)initView {
    // init self variables
    serverNameArray = [[NSMutableArray alloc] initWithObjects:LocalizedStringFromBundle(kLocaleBundle, @"ADVANCED_AUTOMATIC", @""), nil];
    currentConfiguredServer = nil;
    selectedServer = nil;
    bChangeServer = NO;
    
    // Get Working Server List
    //[self callServerList];
    
    // init Server List Drop down picker
    self.ddServer = [[DropDown alloc] init];
    
    self.serverList = [[NSMutableArray alloc] initWithArray:[MyUtils loadCurrentWorkingServers:USERDEFAULTS_CURRENT_WORKING_SERVERS]];
    if ([self.serverList count] > 0) {
        for (BlackCapServer *server in self.serverList) {
            [serverNameArray addObject:server.name];
        }
        self.ddServer.dataSource = serverNameArray;
        selectedServer = [self.serverList firstObject];
    } else {
        [self showAlertView:APPNAME
                    message:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_NO_WORKING_SERVERS", @"")
                     button:LocalizedStringFromBundle(kLocaleBundle, @"LOG_VIEW_CLOSE", @"")
                    handler:nil];
    }
    
    self.ddServer.anchorView = self.anchorLabel;
    self.ddServer.dataSource = serverNameArray;
    [self.btnServerList setImageEdgeInsets:UIEdgeInsetsMake(0, kDeviceWidth - 120, 0, 0)];
    [self.ddServer setSelectionAction:^(NSInteger index, NSString * _Nonnull serverName) {
        [self.btnServerList setTitle:serverName forState:UIControlStateNormal];
        if (index == 0) {
            selectedServer = [self.serverList firstObject];
        } else {
            selectedServer = [self.serverList objectAtIndex:index-1];
        }
    }];
    
    // Menu Button add click event for sidmenu
    [self customSetup];

    // Connect and Disconnect button theme when clicking
    [self.btnConnect.layer setBorderWidth:2.0];
    [self.btnConnect.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.btnConnect setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.btnConnect setBackgroundColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.btnConnect setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnConnect setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    
}

- (void)initViewStrings {
    [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"MAIN_CONNECT", @"") forState:UIControlStateNormal];
    [self.btnDisconnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"MAIN_DISCONNECT", @"") forState:UIControlStateNormal];
    [self.btnAutomatic setTitle:LocalizedStringFromBundle(kLocaleBundle, @"MAIN_AUTOMATIC", @"") forState:UIControlStateNormal];

    self.lblSelectServer.text = LocalizedStringFromBundle(kLocaleBundle, @"MAIN_SELECT_SERVER", @"");
    self.lblConnection.text = LocalizedStringFromBundle(kLocaleBundle, @"MAIN_YOUR_CONNECTION", @"");
    self.lblLocation.text = LocalizedStringFromBundle(kLocaleBundle, @"MAIN_LOCATION", @"");
    self.lblIpAddress.text = LocalizedStringFromBundle(kLocaleBundle, @"MAIN_IP_ADDRESS", @"");
    
}

- (void)handleTodayWidgetURL {
    BlackCapServer *configuringServer = [MyUtils loadCustomObjectWithKey:USERDEFAULTS_TODAY_CONFIGURING_SERVER];
    if (configuringServer) {
        [self.btnServerList setTitle:configuringServer.name forState:UIControlStateNormal];
        [self connectOpenVPN:configuringServer];
        [kUserDefaults removeObjectForKey:USERDEFAULTS_TODAY_CONFIGURING_SERVER];
        [kUserDefaults removeObjectForKey:USERDEFAULTS_TODAY_URL];
        [BlackCapVPNCommon insertLogIntoDataBaseWithLog:[NSString stringWithFormat:@"%@ - %@ - %@", LocalizedStringFromBundle(kLocaleBundle, @"BASIC_OPENVPN_CONNECTED", @""), configuringServer.country, configuringServer.ip]];
    }
}

- (void)connectIPSec:(BlackCapServer*)server {
    BlackCapVPNIPSecConfig *config = [BlackCapVPNIPSecConfig new];
    config.username = [kUserDefaults objectForKey:USERDEFAULTS_USERNAME];
    config.password = [kUserDefaults objectForKey:USERDEFAULTS_PASSWORD];
    config.sharePrivateKey = @"pgxvpn";
    
    [[kSharedAppdelegate vpnManager] setConfig:config andServer:server];
    [[kSharedAppdelegate vpnManager] saveConfigCompleteHandle:^(BOOL success, NSString *returnInfo) {
        if (success) {
            NSLog(@"config IPSec success");
            [[kSharedAppdelegate vpnManager] start];
        }
        else
        {
            NSLog(@"config IPSec error:%@", returnInfo);
        }
    }];
}

- (void)connectL2TP:(BlackCapServer*)server {
    BlackCapVPNL2TPConfig *config = [BlackCapVPNL2TPConfig new];
    config.username = [kUserDefaults objectForKey:USERDEFAULTS_USERNAME];
    config.password = [kUserDefaults objectForKey:USERDEFAULTS_PASSWORD];
    config.sharePrivateKey = @"pgxvpn";
    
    [[kSharedAppdelegate vpnManager] setConfig:config andServer:server];
    [[kSharedAppdelegate vpnManager] saveConfigCompleteHandle:^(BOOL success, NSString *returnInfo) {
        if (success) {
            NSLog(@"config L2TP success");
            [[kSharedAppdelegate vpnManager] start];
        }
        else
        {
            NSLog(@"config L2TP error:%@", returnInfo);
        }
    }];
}

- (void)connectOpenVPN:(BlackCapServer*)server {
    BlackCapVPNOpenVPNConfig *config = [BlackCapVPNOpenVPNConfig new];
    config.username = [kUserDefaults objectForKey:USERDEFAULTS_USERNAME];
    config.password = [kUserDefaults objectForKey:USERDEFAULTS_PASSWORD];

    [[kSharedAppdelegate vpnManager] setConfig:config andServer:server];
    [[kSharedAppdelegate vpnManager] saveConfigCompleteHandle:^(BOOL success, NSString *returnInfo) {
        if (success) {
            NSLog(@"config OpenVPN success");
            [MyUtils saveCustomObject:server key:USERDEFAULTS_CONFIGURED_SERVER];
            [[kSharedAppdelegate vpnManager] start];
        }
        else
        {
            NSLog(@"config OpenVPN error:%@", returnInfo);
        }
    }];
}

- (void)makeDisconnectButtonEnable:(BOOL)enable {
    if (enable) {
        [self.btnDisconnect setEnabled:YES];
        [self.btnDisconnect.layer setBorderWidth:2.0];
        [self.btnDisconnect.layer setBorderColor:[UIColor blackColor].CGColor];
        
        [self.btnDisconnect setBackgroundColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.btnDisconnect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    } else {
        [self.btnDisconnect.layer setBorderWidth:0.0];
        [self.btnDisconnect setBackgroundColor:COLOR(225)];
        [self.btnDisconnect setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.btnDisconnect setEnabled:NO];
    }
}

- (void)changeButtonsForState:(NEVPNStatus)status {
    switch (status) {
        case NEVPNStatusConnecting:
            [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CONNECTING", @"") forState:UIControlStateNormal];
            break;
        case NEVPNStatusConnected:
            [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"ADVANCED_CHANGE_SERVER", @"") forState:UIControlStateNormal];
            [self makeDisconnectButtonEnable:YES];
            break;
        case NEVPNStatusDisconnecting:
            [self.btnDisconnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_DISCONNECTING", @"") forState:UIControlStateNormal];
            break;
        case NEVPNStatusDisconnected:
            [BlackCapVPNCommon insertLogIntoDataBaseWithLog:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_OPENVPN_DISCONNECTED", @"")];
            [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CONNECT", @"") forState:UIControlStateNormal];
            [self.btnDisconnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_DISCONNECT", @"") forState:UIControlStateNormal];
            [self makeDisconnectButtonEnable:NO];
            if (bChangeServer) {
                bChangeServer = NO;
                [self connectOpenVPN:selectedServer];
            }
            break;
        case NEVPNStatusReasserting:
            [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_RECONNECTING", @"") forState:UIControlStateNormal];
            break;
        case NEVPNStatusInvalid:
            NSLog(@"%@", @"Vpn state is invalid.");
            break;
    }
    [self showConnection:status];
}

- (void)showConnection:(NEVPNStatus)status {
    if (status == NEVPNStatusConnected) {
        BlackCapServer *configuredServer = [MyUtils loadCustomObjectWithKey:USERDEFAULTS_CONFIGURED_SERVER];
        if (configuredServer) {
            self.tfLocation.text = configuredServer.country;
            self.tfIPAddress.text = configuredServer.ip;
            [BlackCapVPNCommon insertLogIntoDataBaseWithLog:[NSString stringWithFormat:@"%@ - %@ - %@", LocalizedStringFromBundle(kLocaleBundle, @"BASIC_OPENVPN_CONNECTED", @""), configuredServer.country, configuredServer.ip]];
        } else {
            self.tfLocation.text = @"-";
            self.tfIPAddress.text = @"-";
        }
    } else {
        self.tfLocation.text = @"-";
        self.tfIPAddress.text = @"-";
    }
    
}

#pragma Sidemenu Button Action
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)showAlertsUpgradePurchase
{
    PurchaseAlertVC *alertVC = [ChooseViewController loadViewControllerWithId:@"PurchaseAlertVC"];
    
    alertVC.providesPresentationContextTransitionStyle = YES;
    alertVC.definesPresentationContext = YES;
    [alertVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    alertVC.modalPresentationStyle = UIModalPresentationFormSheet | UIModalPresentationOverCurrentContext;
    alertVC.isVPN = YES;
    [self.navigationController presentViewController:alertVC animated:NO completion:nil];
}

#pragma mark - Button Action Methods
- (IBAction)btnMenuAction:(id)sender {
}

- (IBAction)btnToggleAction:(id)sender {
    UINavigationController *basicNav = [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"BasicViewController"]];
    [basicNav setNavigationBarHidden:YES];
    [self.revealViewController pushFrontViewController:basicNav animated:YES];
}

- (IBAction)btnConnectAction:(id)sender {
    //[[Crashlytics sharedInstance] crash]; // Force crash for testing
    
    if (![kUserDefaults boolForKey:USERDEFAULTS_LOGIN_STATUS_VPN]) {
        [self showAlertsUpgradePurchase];
        return;
    }
    
    switch ([kSharedAppdelegate vpnManager].status) {
        case NEVPNStatusConnecting:
        {
            [self showAlertView:LocalizedStringFromBundle(kLocaleBundle, @"QUESTION_CANCEL_CONNECTION", @"")
                        message:LocalizedStringFromBundle(kLocaleBundle, @"MSG_CANCEL_CONNECTING", @"")
                       okButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CANCEL_CONNECTION", @"")
                   cancelButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_STAY", @"")
                      okHandler:^(UIAlertAction * _Nonnull action) {
                          [[kSharedAppdelegate vpnManager] stop];
                      } cancelHandler:nil];
        }
            break;
        case NEVPNStatusConnected:
        {
            [self showAlertView:@"Change Server?"
                        message:LocalizedStringFromBundle(kLocaleBundle, @"MSG_CHANGE_SERVER", @"")
                       okButton:@"CHANGE CONNECTION"
                   cancelButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CANCEL", @"")
                      okHandler:^(UIAlertAction * _Nonnull action) {
                          bChangeServer = YES;
                          [[kSharedAppdelegate vpnManager] stop];
                      } cancelHandler:nil];
        }
            break;
        case NEVPNStatusDisconnecting:
            break;
        case NEVPNStatusDisconnected:
            [self connectOpenVPN:selectedServer];
            break;
        case NEVPNStatusReasserting:
        {
            [self showAlertView:LocalizedStringFromBundle(kLocaleBundle, @"QUESTION_CANCEL_CONNECTION", @"")
                        message:LocalizedStringFromBundle(kLocaleBundle, @"MSG_CANCEL_CONNECTING", @"")
                       okButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CANCEL_CONNECTION", @"")
                   cancelButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_STAY", @"")
                      okHandler:^(UIAlertAction * _Nonnull action) {
                          [[kSharedAppdelegate vpnManager] stop];
                      } cancelHandler:nil];
        }
            break;
        case NEVPNStatusInvalid:
            [self connectOpenVPN:selectedServer];
            break;
    }
}

- (IBAction)btnDisconnectAction:(id)sender {
    switch ([kSharedAppdelegate vpnManager].status) {
        case NEVPNStatusConnecting:
            break;
        case NEVPNStatusConnected:
        {
            [self showAlertView:LocalizedStringFromBundle(kLocaleBundle, @"QUESTION_BECOME_VISIBLE", @"")
                        message:LocalizedStringFromBundle(kLocaleBundle, @"MSG_BECOME_VISIBLE", @"")
                       okButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_BECOME_VISIBLE", @"")
                   cancelButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CANCEL", @"")
                      okHandler:^(UIAlertAction * _Nonnull action) {
                          [[kSharedAppdelegate vpnManager] stop];
                      } cancelHandler:nil];
        }
            break;
        case NEVPNStatusDisconnecting:
            break;
        case NEVPNStatusDisconnected:
            [self connectOpenVPN:selectedServer];
            break;
        case NEVPNStatusReasserting:
            break;
        case NEVPNStatusInvalid:
            [self connectOpenVPN:selectedServer];
            break;
    }
}

- (IBAction)btnServerListAction:(id)sender {
    [self.ddServer show];
}

@end




