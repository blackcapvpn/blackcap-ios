//
//  MenuViewController.m
//  Blackcap
//
//  Created by rstar on 6/22/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "MenuViewController.h"
#import "SideMenuCell.h"
#import "WebViewController.h"
#import "Environment.h"
#import "RegistrationViewController.h"
#import "MyBlackcap-Swift.h"
#import "SignalsNavigationController.h"
#import "ViewControllerUtils.h"
#import <SignalServiceKit/TSAccountManager.h>
#import <SignalServiceKit/TSDatabaseView.h>

@interface MenuViewController ()
{
    NSArray *imgArray, *dataArray, *lineArray;
}
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
}

- (void)initView {
    //[self.contentView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    dataArray = [[NSArray alloc] initWithObjects:
                 LocalizedStringFromBundle(kLocaleBundle, @"MENU_HOME", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"MENU_ADVANCED", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"MENU_ENCRYPTED_MESSENGER", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SUPPORT", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"MENU_LEGAL_NOTICE", @""),
                 LocalizedStringFromBundle(kLocaleBundle, @"MESSAGE_HOME_MENU_SETTINGS", @""),
                 @"English/Español",
                 LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_LOGOUT", @""),
                 nil];
    
    imgArray = [[NSArray alloc] initWithObjects:
                @"icon_home",
                @"icon_advanced",
                @"icon_messenger",
                @"icon_support",
                @"icon_warning",
                @"icon_settings",
                @"icon_language",
                @"icon_logout",
                nil];
    
    lineArray = [[NSArray alloc] initWithObjects:@"no", @"no", @"no", @"no", @"no", @"no", @"no", @"no", nil];

    [self.menuTableView reloadData];
}

#pragma mark - tableview datasource and delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"sideMenuCell";
    SideMenuCell *cell=(SideMenuCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"SideMenuCell" owner:nil options:nil];
        cell=[nib objectAtIndex:0];
    }
    cell.m_label.text = [dataArray objectAtIndex:indexPath.row];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [cell.icon setImage:[UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]]];
    cell.icon.image = [cell.icon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.icon setTintColor:COLOR(10)];
    
    if ([[lineArray objectAtIndex:indexPath.row] isEqualToString:@"no"]){
        cell.line_label.hidden = YES;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:210.0/255.0 blue:200.0/255.0 alpha:0.3];
    [cell setSelectedBackgroundView:bgColorView];
    cell.m_label.highlightedTextColor = [UIColor whiteColor];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    NSLog(@"Row : %ld", (long)indexPath.row);
    
    SWRevealViewController *revealController = self.revealViewController;
    if (indexPath.row == 0)
    {
        UINavigationController *basicNav = [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"BasicViewController"]];
        [basicNav setNavigationBarHidden:YES];
        [revealController pushFrontViewController:basicNav animated:YES];
    }
    else if (indexPath.row == 1)
    {
        UINavigationController *advancedNav = [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"AdvancedViewController"]];
        [advancedNav setNavigationBarHidden:YES];
        [revealController pushFrontViewController:advancedNav animated:YES];
    }
    else if (indexPath.row == 2)
    {
        if ([Environment.preferences requirePIN]) {
            UINavigationController *messageNav = [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"ConfirmPinViewController"]];
            [messageNav setNavigationBarHidden:YES];
            [revealController pushFrontViewController:messageNav animated:YES];
        } else {
            [self openSignalViewController];
        }
    }
    else if (indexPath.row == 3)
    {
        WebViewController *webVC = [ChooseViewController loadViewControllerWithId:@"WebViewController"];
        webVC.titleStr = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SUPPORT", @"");
        webVC.webViewUrl = @"members/supporttickets.php";
        [(UINavigationController*)revealController.frontViewController pushViewController:webVC animated:YES];
    }
    else if (indexPath.row == 4)
    {
        UIViewController *vc = [ChooseViewController loadViewControllerWithId:@"LegalViewController"];
        [(UINavigationController*)revealController.frontViewController pushViewController:vc animated:YES];
    }
    else if (indexPath.row == 5)
    {
        UIViewController *settingsVC = [ChooseViewController loadViewControllerWithId:@"SettingsViewController"];
        [(UINavigationController*)revealController.frontViewController pushViewController:settingsVC animated:YES];
    }
    else if (indexPath.row == 6)
    {
        UIViewController *languageVC = [ChooseViewController loadViewControllerWithId:@"LanguageVC"];
        [(UINavigationController*)revealController.frontViewController pushViewController:languageVC animated:YES];
    }
    else if (indexPath.row == 7)
    {
        [kUserDefaults removeObjectForKey:USERDEFAULTS_USERNAME];
        [kUserDefaults removeObjectForKey:USERDEFAULTS_PASSWORD];
        UINavigationController *loginNav = [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"LoginViewController"]];
        [loginNav setNavigationBarHidden:YES];
        [revealController presentViewController:loginNav animated:YES completion:nil];
    }
    [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.backgroundColor = (__bridge CGColorRef _Nullable)([UIColor clearColor]);
    cell.backgroundView = nil;
}

- (void)openSignalViewController
{
    
    SWRevealViewController *revealController = self.revealViewController;
    if ([TSAccountManager isRegistered]) {
        HomeViewController *homeView = [HomeViewController new];
        SignalsNavigationController *navigationController = [[SignalsNavigationController alloc] initWithRootViewController:homeView];
        [revealController pushFrontViewController:navigationController animated:YES];
    } else {
        RegistrationViewController *viewController = [RegistrationViewController new];
        OWSNavigationController *navigationController = [[OWSNavigationController alloc] initWithRootViewController:viewController];
        navigationController.navigationBarHidden = YES;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    
}


@end

