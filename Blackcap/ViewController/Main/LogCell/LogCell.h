//
//  LogCell.h
//  Blackcap
//
//  Created by rstar on 6/28/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblLog;

@end
