//
//  SettingsCell.h
//  Blackcap
//
//  Created by rstar on 6/24/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *m_label;
@property (weak, nonatomic) IBOutlet UILabel *line_label;
@property (weak, nonatomic) IBOutlet BEMCheckBox *checkBox;
@property (weak, nonatomic) IBOutlet UIButton *btnCell;

@end
