//
//  SettingsCell.m
//  Blackcap
//
//  Created by rstar on 6/24/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "SettingsCell.h"

@implementation SettingsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.checkBox.boxType = BEMBoxTypeSquare;
    
}

#pragma mark - button action methods
- (IBAction)btnCellClicked:(id)sender {
    if (self.checkBox.on) {
        [self.checkBox setOn:NO animated:YES];
        [BlackCapVPNCommon insertLogIntoDataBaseWithLog:[NSString stringWithFormat:@"%@ %@", self.m_label.text, LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_CELL_SELECTED", @"")]];
    } else {
        [self.checkBox setOn:YES animated:YES];
        [BlackCapVPNCommon insertLogIntoDataBaseWithLog:[NSString stringWithFormat:@"%@ %@", self.m_label.text, LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_CELL_DESELECTED", @"")]];
    }
    [kUserDefaults setBool:self.checkBox.on forKey:self.m_label.text];
    NSLog(@"Auto Reconnect:%i", self.checkBox.on);
    [kUserDefaults synchronize];
    
}

@end
