//
//  PurchaseAlertVC.h
//  Blackcap
//
//  Created by RStar on 3/20/19.
//  Copyright © 2019 rstar. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PurchaseAlertVC : UIViewController

@property BOOL isVPN;
@end

NS_ASSUME_NONNULL_END
