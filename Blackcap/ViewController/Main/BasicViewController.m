//
//  BasicViewController.m
//  Blackcap
//
//  Created by rstar on 6/22/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "BasicViewController.h"
#import "PurchaseAlertVC.h"

@interface BasicViewController ()
{
    BlackCapServer *suggestServer;
}

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnConnect;
@property (weak, nonatomic) IBOutlet UITextField *tfLocation;
@property (weak, nonatomic) IBOutlet UITextField *tfIPAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnVideoStreaming;
@property (weak, nonatomic) IBOutlet UIButton *btnWebBrowsing;
@property (weak, nonatomic) IBOutlet UIButton *btnLargeDownloads;
@property (weak, nonatomic) IBOutlet UILabel *lblConnection;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblIpAddress;

//@property (nonatomic, strong) BlackCapVPNManager *vpnManager;

@end

@implementation BasicViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initView];
    [self initViewStrings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[kSharedAppdelegate vpnManager] loadOpenVPNTunnelProviderManager:^(BOOL success, NSString *returnInfo) {
        if (success) {
            [[kSharedAppdelegate vpnManager] mp_NEVPNStatusChanged:^(enum NEVPNStatus status) {
                [self changeButtonsForState:status];
            }];
            [self changeButtonsForState:[[kSharedAppdelegate vpnManager] status]];
        } else {
            NSLog(@"%@", returnInfo);
        }
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[kSharedAppdelegate vpnManager] mp_RemoveNEVPNStatusChangedNotification];
}

#pragma mark - self methods
- (void)initView {
    // Menu Button add click event for sidmenu
    [self customSetup];
    
    // Login button theme when clicking
    [self.btnConnect.layer setBorderWidth:2.0];
    [self.btnConnect.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.btnConnect setBackgroundColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.btnConnect setBackgroundColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.btnConnect setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnConnect setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    
    self.btnWebBrowsing.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnWebBrowsing.titleLabel.numberOfLines = 2;
    self.btnWebBrowsing.titleLabel.textAlignment = NSTextAlignmentCenter;

    self.btnLargeDownloads.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnLargeDownloads.titleLabel.numberOfLines = 2;
    self.btnLargeDownloads.titleLabel.textAlignment = NSTextAlignmentCenter;

    self.btnVideoStreaming.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnVideoStreaming.titleLabel.numberOfLines = 2;
    self.btnVideoStreaming.titleLabel.textAlignment = NSTextAlignmentCenter;
    
}

- (void)initViewStrings {
    [self.btnWebBrowsing setTitle:LocalizedStringFromBundle(kLocaleBundle, @"HOME_WEB_BROWSING", @"") forState:UIControlStateNormal];
    [self.btnLargeDownloads setTitle:LocalizedStringFromBundle(kLocaleBundle, @"HOME_LARGE_DOWNLOADS", @"") forState:UIControlStateNormal];
    [self.btnVideoStreaming setTitle:LocalizedStringFromBundle(kLocaleBundle, @"HOME_VIDEO_STREAMING", @"") forState:UIControlStateNormal];
    [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"MAIN_CONNECT", @"") forState:UIControlStateNormal];
    self.lblConnection.text = LocalizedStringFromBundle(kLocaleBundle, @"MAIN_YOUR_CONNECTION", @"");
    self.lblLocation.text = LocalizedStringFromBundle(kLocaleBundle, @"MAIN_LOCATION", @"");
    self.lblIpAddress.text = LocalizedStringFromBundle(kLocaleBundle, @"MAIN_IP_ADDRESS", @"");
}

- (void)changeButtonsForState:(NEVPNStatus)status {
    switch (status) {
        case NEVPNStatusConnecting:
            [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CONNECTING", @"") forState:UIControlStateNormal];
            break;
        case NEVPNStatusConnected:
            [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_DISCONNECT", @"") forState:UIControlStateNormal];
            break;
        case NEVPNStatusDisconnecting:
            [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_DISCONNECTING", @"") forState:UIControlStateNormal];
            break;
        case NEVPNStatusDisconnected:
            [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CONNECT", @"") forState:UIControlStateNormal];
            [BlackCapVPNCommon insertLogIntoDataBaseWithLog:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_OPENVPN_DISCONNECTED", @"")];
            break;
        case NEVPNStatusReasserting:
            [self.btnConnect setTitle:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_RECONNECTING", @"") forState:UIControlStateNormal];
            break;
        case NEVPNStatusInvalid:
            NSLog(@"%@", @"Vpn state is invalid.");
            break;
    }
    [self showConnection:status];
}

- (void)connectOpenVPN:(BlackCapServer*)server {
    BlackCapVPNOpenVPNConfig *config = [BlackCapVPNOpenVPNConfig new];
    config.username = [kUserDefaults objectForKey:USERDEFAULTS_USERNAME];
    config.password = [kUserDefaults objectForKey:USERDEFAULTS_PASSWORD];
    
    [[kSharedAppdelegate vpnManager] setConfig:config andServer:server];
    [[kSharedAppdelegate vpnManager] saveConfigCompleteHandle:^(BOOL success, NSString *returnInfo) {
        if (success) {
            NSLog(@"config OpenVPN success");
            [MyUtils saveCustomObject:server key:USERDEFAULTS_CONFIGURED_SERVER];
            [[kSharedAppdelegate vpnManager] start];
        }
        else
        {
            NSLog(@"config OpenVPN error:%@", returnInfo);
        }
    }];
}

- (void)showConnection:(NEVPNStatus)status {
    if (status == NEVPNStatusConnected) {
        BlackCapServer *configuredServer = [MyUtils loadCustomObjectWithKey:USERDEFAULTS_CONFIGURED_SERVER];
        if (configuredServer) {
            self.tfLocation.text = configuredServer.country;
            self.tfIPAddress.text = configuredServer.ip;
            [BlackCapVPNCommon insertLogIntoDataBaseWithLog:[NSString stringWithFormat:@"%@ - %@ - %@", LocalizedStringFromBundle(kLocaleBundle, @"BASIC_OPENVPN_CONNECTED", @""), configuredServer.country, configuredServer.ip]];
            
        } else {
            self.tfLocation.text = @"-";
            self.tfIPAddress.text = @"-";
        }
    } else {
        self.tfLocation.text = @"-";
        self.tfIPAddress.text = @"-";
    }
    
}

- (UILocalNotification *)createLocalNotification {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    if (notification) {
        notification.fireDate = [[NSDate date] dateByAddingTimeInterval:10];
        notification.alertTitle = @"Alert Title";
        notification.alertBody = @"Alert Body";
        
        //notification.timeZone = [NSTimeZone defaultTimeZone];
        //notification.applicationIconBadgeNumber = 1;
        //notification.soundName = UILocalNotificationDefaultSoundName;
        notification.repeatInterval = NSCalendarUnitMinute;
    }
    notification.alertBody = @"Custom text";
    return notification;
}

#pragma Sidemenu Button Action
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark - Button Action Methods
- (IBAction)btnMenuAction:(id)sender {
}

- (IBAction)btnToggleAction:(id)sender {
    UINavigationController *advancedNav = [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"AdvancedViewController"]];
    [advancedNav setNavigationBarHidden:YES];
    [self.revealViewController pushFrontViewController:advancedNav animated:YES];
}

- (IBAction)btnDownloadAction:(id)sender {
//    UIViewController *alertVC = [ChooseViewController loadViewControllerWithId:@"PurchaseAlertVC"];
//    
//    alertVC.providesPresentationContextTransitionStyle = YES;
//    alertVC.definesPresentationContext = YES;
//    [alertVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
//    alertVC.modalPresentationStyle = UIModalPresentationFormSheet | UIModalPresentationOverCurrentContext;
//    [self.navigationController presentViewController:alertVC animated:NO completion:nil];
    
}

- (void)showAlertsUpgradePurchase
{
    PurchaseAlertVC *alertVC = [ChooseViewController loadViewControllerWithId:@"PurchaseAlertVC"];
    
    alertVC.providesPresentationContextTransitionStyle = YES;
    alertVC.definesPresentationContext = YES;
    [alertVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    alertVC.modalPresentationStyle = UIModalPresentationFormSheet | UIModalPresentationOverCurrentContext;
    alertVC.isVPN = YES;
    [self.navigationController presentViewController:alertVC animated:NO completion:nil];
}

- (IBAction)btnConnectAction:(id)sender {
    if (![kUserDefaults boolForKey:USERDEFAULTS_LOGIN_STATUS_VPN]) {
        [self showAlertsUpgradePurchase];
        return;
    }
    
    switch ([kSharedAppdelegate vpnManager].status) {
        case NEVPNStatusConnecting:
        {
            [self showAlertView:LocalizedStringFromBundle(kLocaleBundle, @"QUESTION_CANCEL_CONNECTION", @"")
                        message:LocalizedStringFromBundle(kLocaleBundle, @"MSG_CANCEL_CONNECTING", @"")
                       okButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CANCEL_CONNECTION", @"")
                   cancelButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_STAY", @"")
                      okHandler:^(UIAlertAction * _Nonnull action) {
                [[kSharedAppdelegate vpnManager] stop];
            } cancelHandler:nil];
        }
            break;
        case NEVPNStatusConnected:
        {
            [self showAlertView:LocalizedStringFromBundle(kLocaleBundle, @"QUESTION_BECOME_VISIBLE", @"")
                        message:LocalizedStringFromBundle(kLocaleBundle, @"MSG_BECOME_VISIBLE", @"")
                       okButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_BECOME_VISIBLE", @"")
                   cancelButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CANCEL", @"")
                      okHandler:^(UIAlertAction * _Nonnull action) {
                          [[kSharedAppdelegate vpnManager] stop];
                      } cancelHandler:nil];
        }
            break;
        case NEVPNStatusDisconnecting:
            break;
        case NEVPNStatusDisconnected:
            [[kSharedAppdelegate vpnManager] start];
            break;
        case NEVPNStatusReasserting:
        {
            [self showAlertView:LocalizedStringFromBundle(kLocaleBundle, @"QUESTION_CANCEL_CONNECTION", @"")
                        message:LocalizedStringFromBundle(kLocaleBundle, @"MSG_CANCEL_CONNECTING", @"")
                       okButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_CANCEL_CONNECTION", @"")
                   cancelButton:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_STAY", @"")
                      okHandler:^(UIAlertAction * _Nonnull action) {
                [[kSharedAppdelegate vpnManager] stop];
            } cancelHandler:nil];
        }
            break;
        case NEVPNStatusInvalid:
            // init self variables
            suggestServer = [[MyUtils loadCurrentWorkingServers:USERDEFAULTS_CURRENT_WORKING_SERVERS] firstObject];
            
            if (suggestServer) {
                [self connectOpenVPN:suggestServer];
            } else {
                [self showAlertView:APPNAME message:LocalizedStringFromBundle(kLocaleBundle, @"BASIC_NO_WORKING_SERVERS", @"") button:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_CLOSE", @"") handler:nil];
            }
            break;
    }
}

- (IBAction)btnWebBrowsingAction:(id)sender {
}

- (IBAction)btnLargeDownloadsAction:(id)sender {
}

- (IBAction)btnVideoStreamingAction:(id)sender {
}

@end

