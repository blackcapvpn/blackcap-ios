//
//  SettingsCell.m
//  Blackcap
//
//  Created by rstar on 6/24/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "LanguageCell.h"

@implementation LanguageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.checkBox.boxType = BEMBoxTypeSquare;
    
}

- (void)updateCheckbox {
    if ([[kUserDefaults objectForKey:USERDEFAULTS_LANGUAGE] isEqualToString:self.m_label.text]) {
        [self.checkBox setOn:YES animated:YES];
    } else {
        [self.checkBox setOn:NO animated:YES];
    }
}

@end
