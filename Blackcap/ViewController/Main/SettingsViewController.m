//
//  SettingsViewController.m
//  Blackcap
//
//  Created by rstar on 6/22/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsCell.h"
#import "WebViewController.h"

@interface SettingsViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSArray *titleArray, *dataArray,*checkArray;
}
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initView];
}

- (void)initView {

    self.lblTitle.text = LocalizedStringFromBundle(kLocaleBundle, @"MENU_SETTINGS", @"");
    titleArray = [[NSArray alloc] initWithObjects:
                  //@"VPN settings",
//                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_PREFERENCES", @""),
//                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_ACCOUNT_OPTIONS", @""),
//                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_OTHER", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_PREFERENCES", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_ACCOUNT_OPTIONS", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_OTHER", @""),
                  nil];
    
    dataArray = [[NSArray alloc] initWithObjects:
                 //[[NSArray alloc] initWithObjects:USERDEFAULTS_DNS_LEAK_PROTECTION, nil],
                 [[NSArray alloc] initWithObjects:
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_AUTO_RECONNECT", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SHOW_NOTIFICATION", @""),
                  nil],
                 [[NSArray alloc] initWithObjects:
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_CLIENT_AREA", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SUPPORT", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_REFRESH", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_LOGOUT", @""),
                  nil],
                 [[NSArray alloc] initWithObjects:
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SHOW_LOG", @""),
                  LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_ABOUT_BLACKCAP", @""),
                  nil],
                 nil];
    
    checkArray = [[NSArray alloc] initWithObjects:
                  //[[NSArray alloc] initWithObjects:@"yes", nil],
                  [[NSArray alloc] initWithObjects:@"yes", @"yes", nil],
                  [[NSArray alloc] initWithObjects:@"no", @"no", @"no", @"no", nil],
                  [[NSArray alloc] initWithObjects:@"no", @"no", nil],
                  nil];
    
    [self.settingsTableView reloadData];
}

#pragma mark - Button Back Action
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview datasource and delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dataArray objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 40)];
    
    UILabel *lblHeaderTitle = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, kDeviceWidth - 45, 15)];
    [lblHeaderTitle setFont:AppRegularFont(14)];
    [lblHeaderTitle setTextColor:COLOR(0)];
    [viewHeader addSubview:lblHeaderTitle];
    [viewHeader setBackgroundColor:COLOR(255)];
    
    [lblHeaderTitle setText:[titleArray objectAtIndex:section]];
    
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 20)];
    
    UILabel *lblFooter = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, kDeviceWidth - 30, 1)];
    [viewFooter addSubview:lblFooter];
    [viewFooter setBackgroundColor:COLOR(255)];

    if (section != [dataArray count] - 1) {
        [lblFooter setBackgroundColor:COLOR(120)];
    }
    return viewFooter;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"SettingsCell";
    SettingsCell *cell=(SettingsCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"SettingsCell" owner:nil options:nil];
        cell=[nib objectAtIndex:0];
    }
    cell.m_label.text = [[dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if ([[[checkArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"yes"]) {
        cell.checkBox.hidden = NO;
        if ([kUserDefaults objectForKey:cell.m_label.text]) {
            [cell.checkBox setOn:[kUserDefaults boolForKey:cell.m_label.text]];
        }
    } else {
        cell.checkBox.hidden = YES;
        cell.btnCell.hidden = YES;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:210.0/255.0 blue:200.0/255.0 alpha:0.3];
    [cell setSelectedBackgroundView:bgColorView];
    
    [cell.btnCell addTarget:cell action:@selector(btnCellClicked:) forControlEvents:UIControlEventTouchDown];
    //cell.m_label.highlightedTextColor = [UIColor whiteColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    NSString *selectedStr = [[dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    if ([selectedStr isEqualToString:USERDEFAULTS_DNS_LEAK_PROTECTION])
    {        
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_AUTO_RECONNECT", @"")])
    {
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SHOW_NOTIFICATION", @"")])
    {
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_CLIENT_AREA", @"")])
    {
        WebViewController *webVC = [ChooseViewController loadViewControllerWithId:@"WebViewController"];
        webVC.titleStr = @"Client Area";
        webVC.webViewUrl = @"members/clientarea.php";
        [self.navigationController pushViewController:webVC animated:YES];
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SUPPORT", @"")])
    {
        WebViewController *webVC = [ChooseViewController loadViewControllerWithId:@"WebViewController"];
        webVC.titleStr = @"Supports";
        webVC.webViewUrl = @"members/supporttickets.php";
        [self.navigationController pushViewController:webVC animated:YES];
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_REFRESH", @"")])
    {
        [self callRefresh];
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_LOGOUT", @"")])
    {
        [kUserDefaults removeObjectForKey:USERDEFAULTS_USERNAME];
        [kUserDefaults removeObjectForKey:USERDEFAULTS_PASSWORD];
        [BlackCapVPNCommon insertLogIntoDataBaseWithLog:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_LOGOUT_SUCCESS", @"")];
        UINavigationController *loginNav = [[UINavigationController alloc] initWithRootViewController:[ChooseViewController loadViewControllerWithId:@"LoginViewController"]];
        [loginNav setNavigationBarHidden:YES];
        [self presentViewController:loginNav animated:YES completion:nil];
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SHOW_LOG", @"")])
    {
        [self.navigationController pushViewController:[ChooseViewController loadViewControllerWithId:@"LogViewController"] animated:YES];
    }
    else if ([selectedStr isEqualToString:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_ABOUT_BLACKCAP", @"")])
    {
        [self showAlertView:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_ABOUT_BLACKCAP", @"")
                    message:LocalizedStringFromBundle(kLocaleBundle, @"MSG_ABOUT_BLACKCAP", @"")
                     button:LocalizedStringFromBundle(kLocaleBundle, @"LOG_VIEW_CLOSE", @"")
                    handler:nil];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.backgroundColor = (__bridge CGColorRef _Nullable)([UIColor clearColor]);//optional
    cell.backgroundView = nil;
}

- (void)callRefresh {
    NSDictionary *dict = @{@"u":[kUserDefaults objectForKey:USERDEFAULTS_USERNAME], @"p":[kUserDefaults objectForKey:USERDEFAULTS_PASSWORD]};
    [ApiManager postDataToURL:SUB_URL_LOGIN withParameter:dict authToken:nil sender:self responseHandler:^(id  _Nullable responseObject, bool isSuccess) {
        int responseCode = [[[responseObject objectForKey:@"meta"] objectForKey:@"code"] intValue];
        if (responseCode == 200) {
            [kUserDefaults setBool:[[[responseObject objectForKey:@"data"] objectForKey:@"vpn"] boolValue] forKey:USERDEFAULTS_LOGIN_STATUS_VPN];
            Boolean bMsg = [[[responseObject objectForKey:@"data"] objectForKey:@"messenger"] boolValue];
            [kUserDefaults setBool:bMsg forKey:USERDEFAULTS_LOGIN_STATUS_MESSENGER];
            int msgCount = bMsg ? 0 : [[[responseObject objectForKey:@"data"] objectForKey:@"message_count"] intValue];
            [kUserDefaults setInteger:msgCount forKey:USERDEFAULTS_FREE_MESSAGE_NUM];
            [kUserDefaults synchronize];
        }
    }];
}
@end


