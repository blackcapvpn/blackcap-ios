//
//  PurchaseAlertVC.m
//  Blackcap
//
//  Created by RStar on 3/20/19.
//  Copyright © 2019 rstar. All rights reserved.
//

#import "PurchaseAlertVC.h"

@interface PurchaseAlertVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *alertFirst;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleFirst;
@property (weak, nonatomic) IBOutlet UILabel *lblDescriptionFirst;
@property (weak, nonatomic) IBOutlet UIButton *btnNoThanks;
@property (weak, nonatomic) IBOutlet UIButton *btnUpgrade;

@property (weak, nonatomic) IBOutlet UIView *alertSecond;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSecond;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondPurchase;
@property (weak, nonatomic) IBOutlet UILabel *lblSubscriber;
@property (weak, nonatomic) IBOutlet UILabel *lblDevices;
@property (weak, nonatomic) IBOutlet UILabel *lblContract;
@property (weak, nonatomic) IBOutlet UILabel *lblAccessGlobal;
@property (weak, nonatomic) IBOutlet UILabel *lblMessenger;
@property (weak, nonatomic) IBOutlet UILabel *lblUnlimited;

@property (weak, nonatomic) IBOutlet UIView *alertThird;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleThird;
@property (weak, nonatomic) IBOutlet UIButton *btnThirdCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnThirdPurchase;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfDate;
@property (weak, nonatomic) IBOutlet UITextField *tfCvc;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;
@property (weak, nonatomic) IBOutlet UILabel *lblRemember;
@property (weak, nonatomic) IBOutlet UIButton *btnRemember;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UIView *viewCard;
@property (weak, nonatomic) IBOutlet UIView *viewRemember;

@property (weak, nonatomic) IBOutlet UIView *alertSuccess;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSuccess;
@property (weak, nonatomic) IBOutlet UILabel *lblDescriptionSuccess;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;

@property (weak, nonatomic) IBOutlet UIView *alertFailed;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleError;
@property (weak, nonatomic) IBOutlet UILabel *lblDescriptionError;
@property (weak, nonatomic) IBOutlet UIButton *btnFailedCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnTryAgain;


@property BOOL isChecked;
@property NSString * mEmail;
@property NSString * mCardNumber;
@property NSString * mCardDate;
@property NSString * mCardCvc;

@end

@implementation PurchaseAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
}

- (void)initView {
    if (self.isVPN) {
        self.lblTitleFirst.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_VPN_PURCHASE_TITLE", @"");
        self.lblDescriptionFirst.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_VPN_PURCHASE_BODY", @"");
    } else {
        self.lblTitleFirst.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_FREE_MESSAGE_TITLE", @"");
        self.lblDescriptionFirst.text = LocalizedStringFromBundle(kLocaleBundle, @"MSG_CANT_USE_MESSENGER", @"");
    }
    
    [self.btnNoThanks setTitle:LocalizedStringFromBundle(kLocaleBundle, @"ALERT_NO_THANKS", @"") forState:UIControlStateNormal];
    [self.btnUpgrade setTitle:LocalizedStringFromBundle(kLocaleBundle, @"ALERT_UPGRADE", @"") forState:UIControlStateNormal];

    self.lblTitleSecond.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_UPGRADE_TITLE", @"");
    self.lblSubscriber.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_SUSCRIPTOR", @"");
    self.lblDevices.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_DEVICES", @"");
    self.lblContract.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_NO_FIXED_CONTRACTS", @"");
    self.lblAccessGlobal.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_ACCESS_GLOBAL", @"");
    self.lblMessenger.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_UNLIMITED_MESSENGING", @"");
    self.lblUnlimited.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_UNLIMITED_DATA_USAGE", @"");
    [self.btnSecondCancel setTitle:LocalizedStringFromBundle(kLocaleBundle, @"TXT_CANCEL_TITLE", @"") forState:UIControlStateNormal];
    [self.btnSecondPurchase setTitle:LocalizedStringFromBundle(kLocaleBundle, @"ALERT_PURCHASE", @"") forState:UIControlStateNormal];

    self.lblTitleThird.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_UPGRADE_TITLE", @"");
    self.lblRemember.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_REMEMBER_ME", @"");
    self.tfEmail.placeholder = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_EMAIL", @"");
    self.tfCardNumber.placeholder = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_CARD_NUMBER", @"");
    self.tfDate.placeholder = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_MM_YY", @"");
    self.tfCvc.placeholder = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_CVC", @"");
    [self.btnThirdCancel setTitle:LocalizedStringFromBundle(kLocaleBundle, @"TXT_CANCEL_TITLE", @"") forState:UIControlStateNormal];
    [self.btnThirdPurchase setTitle:LocalizedStringFromBundle(kLocaleBundle, @"ALERT_PURCHASE", @"") forState:UIControlStateNormal];

    self.lblTitleSuccess.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_PAYMENT_SUCCESS_TITLE", @"");
    self.lblDescriptionSuccess.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_PAYMENT_SUCCESS_DESCRIPTION", @"");
    [self.btnContinue setTitle:LocalizedStringFromBundle(kLocaleBundle, @"ALERT_CONTINUE", @"") forState:UIControlStateNormal];

    self.lblTitleError.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_TRANSACTION_ERROR_TITLE", @"");
    self.lblDescriptionError.text = LocalizedStringFromBundle(kLocaleBundle, @"ALERT_TRANSACTION_ERROR_DESCRIPTION", @"");
    [self.btnFailedCancel setTitle:LocalizedStringFromBundle(kLocaleBundle, @"TXT_CANCEL_TITLE", @"") forState:UIControlStateNormal];
    [self.btnTryAgain setTitle:LocalizedStringFromBundle(kLocaleBundle, @"REGISTER_FAILED_TRY_AGAIN", @"") forState:UIControlStateNormal];

    _isChecked = false;
    
    self.alertFirst.layer.cornerRadius = 12;
    self.alertFirst.clipsToBounds = YES;
    self.alertSecond.layer.cornerRadius = 12;
    self.alertSecond.clipsToBounds = YES;
    self.alertThird.layer.cornerRadius = 12;
    self.alertThird.clipsToBounds = YES;
    self.alertSuccess.layer.cornerRadius = 12;
    self.alertSuccess.clipsToBounds = YES;
    self.alertFailed.layer.cornerRadius = 12;
    self.alertFailed.clipsToBounds = YES;
    self.viewEmail.layer.cornerRadius = 3;
    self.viewEmail.clipsToBounds = YES;
    self.viewEmail.layer.borderWidth = 0.5;
    self.viewEmail.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.viewCard.layer.cornerRadius = 3;
    self.viewCard.clipsToBounds = YES;
    self.viewCard.layer.borderWidth = 0.5;
    self.viewCard.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.viewRemember.layer.cornerRadius = 3;
    self.viewRemember.clipsToBounds = YES;
    self.viewRemember.layer.borderWidth = 0.5;
    self.viewRemember.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.mEmail = [kUserDefaults objectForKey:USERDEFAULTS_USERNAME];
    if (self.mEmail) {
        [self.tfEmail setText:self.mEmail];
    }
}

- (IBAction)btnNoThanksClicked:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnUpgradeClicked:(id)sender {
    [self.alertFirst setHidden:YES];
    [self.alertSecond setHidden:NO];
}

- (IBAction)btnSecondCancelClicked:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnSecondPurchaseClicked:(id)sender {
    [self.alertSecond setHidden:YES];
    [self.alertThird setHidden:NO];
}

- (IBAction)btnThirdCancelClicked:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnThirdPurchaseClicked:(id)sender {
    if ([self isValidInput]) {
        [self.view endEditing:YES];
        
        [self callPurchase];
    }

}

- (IBAction)btnRememberClicked:(id)sender {
    self.isChecked = !self.isChecked;
    self.imgCheck.image = self.isChecked ? [UIImage imageNamed:@"icon_checked"] : [UIImage imageNamed:@"icon_unchecked"];
}

- (IBAction)btnContinueClicked:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnFailedCancelClicked:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnTryAgainClicked:(id)sender {
    self.tfCardNumber.text = @"";
    self.tfDate.text = @"";
    self.tfCvc.text = @"";
    
    [self.alertFailed setHidden:YES];
    [self.alertSecond setHidden:NO];
}

- (BOOL)isValidInput {
    if ([self.tfEmail.text length]) {
        self.mEmail = self.tfEmail.text;
    } else {
        [self.tfEmail becomeFirstResponder];
        return NO;
    }
    
    if ([self.tfCardNumber.text length]) {
        self.mCardNumber = self.tfCardNumber.text;
    } else {
        [self.tfCardNumber becomeFirstResponder];
        return NO;
    }
    
    if ([self.tfDate.text length]) {
        self.mCardDate = self.tfDate.text;
    } else {
        [self.tfDate becomeFirstResponder];
        return NO;
    }
    
    if ([self.tfCvc.text length]) {
        self.mCardCvc = self.tfCvc.text;
    } else {
        [self.tfCvc becomeFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.tfDate.text.length == 4 && string.length == 0) {
        self.tfDate.text = [self.tfDate.text substringToIndex:2];
        return NO;
    }

    if ([string isEqualToString:@""]) return YES;
    if (self.tfDate.text.length > 4) return NO;
    
    if (self.tfDate.text.length == 2) {
        self.tfDate.text = [NSString stringWithFormat:@"%@/%@", self.tfDate.text, string];
        return NO;
    }
    
    return YES;
}

#pragma mark - Call APIs
- (void)callPurchase {
    NSString *password = [kUserDefaults objectForKey:USERDEFAULTS_PASSWORD];
    
    NSDictionary *dict = @{@"u":self.mEmail, @"p":password, @"card":self.mCardNumber, @"expdate":self.mCardDate, @"cvv":self.mCardCvc};

    [ApiManager postDataToURL:SUB_URL_UPGRADE_PURCHASE withParameter:dict authToken:nil sender:self responseHandler:^(id  _Nullable responseObject, bool isSuccess) {
        if ([[[responseObject objectForKey:@"meta"] objectForKey:@"code"] intValue] == 200) {
            [kUserDefaults setObject:self.mCardNumber forKey:USERDEFAULTS_CARD_NUMBER];
            [kUserDefaults setObject:self.mCardDate forKey:USERDEFAULTS_CARD_EXPDATE];
            [kUserDefaults setObject:self.mCardCvc forKey:USERDEFAULTS_CARD_CVV];
            
            [kUserDefaults setBool:YES forKey:USERDEFAULTS_LOGIN_STATUS_VPN];
            [kUserDefaults setBool:YES forKey:USERDEFAULTS_LOGIN_STATUS_MESSENGER];
            [kUserDefaults synchronize];

            [self.alertThird setHidden:YES];
            [self.alertSuccess setHidden:NO];
        } else {
            [self.alertThird setHidden:YES];
            [self.alertFailed setHidden:NO];
        }
    }];
}

@end
