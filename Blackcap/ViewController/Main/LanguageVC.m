//
//  LanguageVC.m
//  Blackcap
//
//  Created by Rstar Dev on 4/11/19.
//  Copyright © 2019 rstar. All rights reserved.
//

#import "LanguageVC.h"
#import "LanguageCell.h"

@interface LanguageVC()<UITableViewDelegate, UITableViewDataSource>
{
    NSArray *titleArray, *dataArray,*checkArray;
}
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;

@end

@implementation LanguageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
}

- (void)initView {
    titleArray = [[NSArray alloc] initWithObjects:
                  LocalizedStringFromBundle(kLocaleBundle, @"LANGUAGE_SELECT", @""),
                  nil];

    dataArray = [[NSArray alloc] initWithObjects:
                 [[NSArray alloc] initWithObjects:
                  @"English",
                  @"Español",
                  nil],
                 nil];
    
    checkArray = [[NSArray alloc] initWithObjects:
                  [[NSArray alloc] initWithObjects:@"yes", @"yes", nil],
                  nil];
    
    [self.settingsTableView reloadData];
}

#pragma mark - Button Back Action
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview datasource and delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dataArray objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 40)];
    
    UILabel *lblHeaderTitle = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, kDeviceWidth - 45, 15)];
    [lblHeaderTitle setFont:AppRegularFont(14)];
    [lblHeaderTitle setTextColor:COLOR(0)];
    [viewHeader addSubview:lblHeaderTitle];
    [viewHeader setBackgroundColor:COLOR(255)];
    
    [lblHeaderTitle setText:[titleArray objectAtIndex:section]];
    
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 20)];
    
    UILabel *lblFooter = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, kDeviceWidth - 30, 1)];
    [viewFooter addSubview:lblFooter];
    [viewFooter setBackgroundColor:COLOR(255)];
    
    if (section != [dataArray count] - 1) {
        [lblFooter setBackgroundColor:COLOR(120)];
    }
    return viewFooter;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"LanguageCell";
    LanguageCell *cell=(LanguageCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"LanguageCell" owner:nil options:nil];
        cell=[nib objectAtIndex:0];
    }
    cell.m_label.text = [[dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if ([[[checkArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"yes"]) {
        cell.checkBox.hidden = NO;
        cell.btnCell.hidden = YES;
        [cell updateCheckbox];
    } else {
        cell.checkBox.hidden = YES;
        cell.btnCell.hidden = YES;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:210.0/255.0 blue:200.0/255.0 alpha:0.3];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    NSString *selectedStr = [[dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    [kUserDefaults setObject:selectedStr forKey:USERDEFAULTS_LANGUAGE];
    [kUserDefaults synchronize];
    
    if ([selectedStr isEqualToString:@"English"]) {
        [kSharedAppdelegate changeLocale:@"en"];
    } else if ([selectedStr isEqualToString:@"Español"]) {
        [kSharedAppdelegate changeLocale:@"es"];
    }

    for (LanguageCell *cell in tableView.visibleCells) {
        [cell updateCheckbox];
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.backgroundColor = (__bridge CGColorRef _Nullable)([UIColor clearColor]);//optional
    cell.backgroundView = nil;
}

@end


