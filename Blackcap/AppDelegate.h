//
//  AppDelegate.h
//  Blackcap
//
//  Created by rstar on 6/19/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^RequestResultHandle)(BOOL success);


@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *MainVC;

@property (nonatomic, strong) BlackCapVPNManager *vpnManager;
@property (atomic) BOOL isEnvironmentSetup;
@property (nonatomic, strong) NSBundle *localeBundle;

- (void)callServerList;
- (void)showUserNotificationWithTitle:(NSString*)title Body:(NSString*)body;
- (void)setupEnvironment;
- (void)changeLocale:(NSString*)locale;
- (void)updateLanguageVisibleViews;
@end

