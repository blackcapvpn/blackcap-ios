//
//  main.m
//  Blackcap
//
//  Created by rstar on 6/19/17.
//  Copyright © 2017 rstar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
