//
//  Copyright (c) 2017 Open Whisper Systems. All rights reserved.
//

import Foundation

@objc
class OWSMessagesBubbleImageFactory: NSObject {

    let jsqFactory = JSQMessagesBubbleImageFactory()!

    // TODO: UIView is a little bit expensive to instantiate.
    //       Can we cache this value?
    @objc
    var isRTL: Bool {
        return UIView().isRTL()
    }

    @objc
    var incoming: JSQMessagesBubbleImage {
        let color = UIColor.jsq_messageBubbleLightGray()!
        return incoming(color: color)
    }

    @objc
    var outgoing: JSQMessagesBubbleImage {
        let color = UIColor.ows_black()
        return outgoing(color: color)
    }

    @objc
    var currentlyOutgoing: JSQMessagesBubbleImage {
        let color = UIColor.ows_fadedBlack()
        return outgoing(color: color)
    }

    @objc
    var outgoingFailed: JSQMessagesBubbleImage {
        let color = UIColor.ows_gray2()
        return outgoing(color: color)
    }

    @objc
    func outgoing(color: UIColor) -> JSQMessagesBubbleImage {
        if isRTL {
            return jsqFactory.incomingMessagesBubbleImage(with: color)
        } else {
            return jsqFactory.outgoingMessagesBubbleImage(with: color)
        }
    }

    @objc
    func incoming(color: UIColor) -> JSQMessagesBubbleImage {
        if isRTL {
            return jsqFactory.outgoingMessagesBubbleImage(with: color)
        } else {
            return jsqFactory.incomingMessagesBubbleImage(with: color)
        }
    }
}
