//
//  Copyright (c) 2017 Open Whisper Systems. All rights reserved.
//

#import "PrivacySettingsTableViewController.h"
#import "BlockListViewController.h"
#import "Environment.h"
#import "OWSPreferences.h"
#import "MyBlackcap-Swift.h"
#import <SignalServiceKit/OWSReadReceiptManager.h>
#import "ConfirmPinViewController.h"
#import "ChangePinViewController.h"

NS_ASSUME_NONNULL_BEGIN

@implementation PrivacySettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SECURITY_TITLE", @"");

    [self updateTableContents];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self updateTableContents];
}

#pragma mark - Table Contents

- (void)updateTableContents
{
    OWSTableContents *contents = [OWSTableContents new];

    __weak PrivacySettingsTableViewController *weakSelf = self;

    [contents
        addSection:[OWSTableSection
                       sectionWithTitle:nil
                                  items:@[
                                      [OWSTableItem disclosureItemWithText:
                                                        LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_BLOCK_LIST_TITLE",
                                                            @"Label for the block list section of the settings view")
                                                               actionBlock:^{
                                                                   [weakSelf showBlocklist];
                                                               }],
                                  ]]];

    OWSTableSection *readReceiptsSection = [OWSTableSection new];
    readReceiptsSection.footerTitle = NSLocalizedString(
        @"SETTINGS_READ_RECEIPTS_SECTION_FOOTER", @"An explanation of the 'read receipts' setting.");
    [readReceiptsSection
        addItem:[OWSTableItem switchItemWithText:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_READ_RECEIPT",
                                                     @"Label for the 'read receipts' setting.")
                                            isOn:[OWSReadReceiptManager.sharedManager areReadReceiptsEnabled]
                                          target:weakSelf
                                        selector:@selector(didToggleReadReceiptsSwitch:)]];
    [contents addSection:readReceiptsSection];

    // PIN Section
    OWSTableSection *pinSection = [OWSTableSection new];
    pinSection.headerTitle = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_PIN_TITLE", @"Section header");
    pinSection.footerTitle = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_PIN_DETAIL", nil);
    [pinSection addItem:[OWSTableItem switchItemWithText:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_PIN_ENABLE_SWITCH", @"")
                                                    isOn:[Environment.preferences requirePIN]
                                                  target:weakSelf
                                                selector:@selector(didTogglePINSwitch:)]];
    
    if ([Environment.preferences requirePIN]) {
        [pinSection addItem:[OWSTableItem disclosureItemWithText:
                             LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_PIN_CHANGE_TITLE",
                                               @"Label for the block list section of the settings view")
                                                     actionBlock:^{
                                                         [weakSelf showChangePinVC];
                                                     }]];
    }
    
    [contents addSection:pinSection];
    
    // Screen Security Section
    OWSTableSection *screenSecuritySection = [OWSTableSection new];
    screenSecuritySection.headerTitle = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SCREEN_SECURITY_TITLE", @"Section header");
    screenSecuritySection.footerTitle = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SCREEN_SECURITY_DETAIL", nil);
    [screenSecuritySection addItem:[OWSTableItem switchItemWithText:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SCREEN_SECURITY", @"")
                                                               isOn:[Environment.preferences screenSecurityIsEnabled]
                                                             target:weakSelf
                                                           selector:@selector(didToggleScreenSecuritySwitch:)]];
    [contents addSection:screenSecuritySection];

    // Allow calls to connect directly vs. using TURN exclusively
//    OWSTableSection *callingSection = [OWSTableSection new];
//    callingSection.headerTitle
//        = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SECTION_TITLE_CALLING", @"settings topic header for table section");
//    callingSection.footerTitle = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_CALLING_HIDES_IP_ADDRESS_PREFERENCE_TITLE_DETAIL",
//        @"User settings section footer, a detailed explanation");
//    [callingSection addItem:[OWSTableItem switchItemWithText:NSLocalizedString(
//                                                                 @"SETTINGS_CALLING_HIDES_IP_ADDRESS_PREFERENCE_TITLE",
//                                                                 @"Table cell label")
//                                                        isOn:[Environment.preferences doCallsHideIPAddress]
//                                                      target:weakSelf
//                                                    selector:@selector(didToggleCallsHideIPAddressSwitch:)]];
//    [contents addSection:callingSection];
//
//    if ([UIDevice currentDevice].supportsCallKit) {
//        OWSTableSection *callKitSection = [OWSTableSection new];
//        callKitSection.footerTitle
//            = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_SECTION_CALL_KIT_DESCRIPTION", @"Settings table section footer.");
//        [callKitSection addItem:[OWSTableItem switchItemWithText:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_PRIVACY_CALLKIT_TITLE",
//                                                                     @"Short table cell label")
//                                                            isOn:[Environment.preferences isCallKitEnabled]
//                                                          target:weakSelf
//                                                        selector:@selector(didToggleEnableCallKitSwitch:)]];
//        if (Environment.preferences.isCallKitEnabled) {
//            [callKitSection
//                addItem:[OWSTableItem switchItemWithText:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_PRIVACY_CALLKIT_PRIVACY_TITLE",
//                                                             @"Label for 'CallKit privacy' preference")
//                                                    isOn:![Environment.preferences isCallKitPrivacyEnabled]
//                                                  target:weakSelf
//                                                selector:@selector(didToggleEnableCallKitPrivacySwitch:)]];
//        }
//        [contents addSection:callKitSection];
//    }

    //    OWSTableSection *historyLogsSection = [OWSTableSection new];
    //    historyLogsSection.headerTitle = LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_HISTORYLOG_TITLE", @"Section header");
    //    [historyLogsSection addItem:[OWSTableItem disclosureItemWithText:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_CLEAR_HISTORY", @"")
    //                                                         actionBlock:^{
    //                                                             [weakSelf clearHistoryLogs];
    //                                                         }]];
    OWSTableSection *historyLogsSection = [OWSTableSection new];
    [historyLogsSection addItem:[OWSTableItem itemWithCustomCellBlock:^{
        UITableViewCell *cell = [UITableViewCell new];
        cell.preservesSuperviewLayoutMargins = YES;
        cell.contentView.preservesSuperviewLayoutMargins = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        const CGFloat kButtonHeight = 40.f;
        OWSFlatButton *button = [OWSFlatButton buttonWithTitle:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_ERASE_ALL_MESSAGE_BUTTON", @"")
                                                          font:[OWSFlatButton fontForHeight:kButtonHeight]
                                                    titleColor:[UIColor whiteColor]
                                               backgroundColor:[UIColor ows_blackColor]
                                                        target:self
                                                      selector:@selector(clearHistoryLogs)];
        [cell.contentView addSubview:button];
        [button autoSetDimension:ALDimensionHeight toSize:kButtonHeight];
        [button autoVCenterInSuperview];
        [button autoPinLeadingAndTrailingToSuperview];
        
        return cell;
    }
                                           customRowHeight:90.f
                                               actionBlock:nil]];
    
    [contents addSection:historyLogsSection];

    self.contents = contents;
}

#pragma mark - Events

- (void)showBlocklist
{
    BlockListViewController *vc = [BlockListViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showChangePinVC
{
    ChangePinViewController *vc = [ChooseViewController loadViewControllerWithId:@"ChangePinViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)clearHistoryLogs
{
    UIAlertController *alertController =
        [UIAlertController alertControllerWithTitle:nil
                                            message:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_DELETE_HISTORYLOG_CONFIRMATION",
                                                        @"Alert message before user confirms clearing history")
                                     preferredStyle:UIAlertControllerStyleAlert];

    [alertController.view setTintColor:[UIColor blackColor]];
    [alertController addAction:[OWSAlerts cancelAction]];

    UIAlertAction *deleteAction =
        [UIAlertAction actionWithTitle:LocalizedStringFromBundle(kLocaleBundle, @"SETTINGS_DELETE_HISTORYLOG_CONFIRMATION_BUTTON", @"")
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *_Nonnull action) {
                                   [[TSStorageManager sharedManager] deleteThreadsAndMessages];
                               }];
    [alertController addAction:deleteAction];

    [self presentViewController:alertController animated:true completion:nil];
}

- (void)didToggleScreenSecuritySwitch:(UISwitch *)sender
{
    BOOL enabled = sender.isOn;
    DDLogInfo(@"%@ toggled screen security: %@", self.logTag, enabled ? @"ON" : @"OFF");
    [Environment.preferences setScreenSecurity:enabled];
}

- (void)didTogglePINSwitch:(UISwitch *)sender
{
    BOOL enabled = sender.isOn;
    DDLogInfo(@"%@ toggled PIN: %@", self.logTag, enabled ? @"ON" : @"OFF");
    [Environment.preferences setRequirePIN:enabled];
    [self updateTableContents];
}

- (void)didToggleReadReceiptsSwitch:(UISwitch *)sender
{
    BOOL enabled = sender.isOn;
    DDLogInfo(@"%@ toggled areReadReceiptsEnabled: %@", self.logTag, enabled ? @"ON" : @"OFF");
    [OWSReadReceiptManager.sharedManager setAreReadReceiptsEnabled:enabled];
}

- (void)didToggleCallsHideIPAddressSwitch:(UISwitch *)sender
{
    BOOL enabled = sender.isOn;
    DDLogInfo(@"%@ toggled callsHideIPAddress: %@", self.logTag, enabled ? @"ON" : @"OFF");
    [Environment.preferences setDoCallsHideIPAddress:enabled];
}

- (void)didToggleEnableCallKitSwitch:(UISwitch *)sender {
    DDLogInfo(@"%@ user toggled call kit preference: %@", self.logTag, (sender.isOn ? @"ON" : @"OFF"));
    [[Environment getCurrent].preferences setIsCallKitEnabled:sender.isOn];
    // rebuild callUIAdapter since CallKit vs not changed.
    [[Environment getCurrent].callService createCallUIAdapter];
    [self updateTableContents];
}

- (void)didToggleEnableCallKitPrivacySwitch:(UISwitch *)sender {
    DDLogInfo(@"%@ user toggled call kit privacy preference: %@", self.logTag, (sender.isOn ? @"ON" : @"OFF"));
    [[Environment getCurrent].preferences setIsCallKitPrivacyEnabled:!sender.isOn];
}

#pragma mark - Log util

+ (NSString *)tag
{
    return [NSString stringWithFormat:@"[%@]", self.class];
}

- (NSString *)tag
{
    return self.class.logTag;
}

@end

NS_ASSUME_NONNULL_END
