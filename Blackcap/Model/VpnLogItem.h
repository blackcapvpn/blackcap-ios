//
//  VpnLogItem.h
//  Blackcap
//
//  Created by Zhang on 18.07.17.
//  Copyright © 2017 rstar. All rights reserved.
//

@interface VpnLogItem : RLMObject

@property NSString *log;
@property NSDate *time;
@end
