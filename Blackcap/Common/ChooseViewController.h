//
//  ChooseViewController.h
//  Revent
//
//  Created by JDev on 11/4/2017.
//  Copyright © 2017 JDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ChooseViewController : NSObject

+ (__kindof UIViewController *)loadViewControllerWithId:(NSString *)viewControllerId;
@end
