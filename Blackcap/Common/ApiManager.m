//
//  ApiManager.m
//  Monastery
//
//  Created by JDev on 14/2/2017.
//  Copyright © 2017 JDev. All rights reserved.
//

#import "ApiManager.h"

@implementation ApiManager


- (NSString *)subUrlFromApiName:(NSString *)apiName param:(NSDictionary *)param{
    NSString *subUrl = apiName;
    if (param) {
        subUrl = [subUrl stringByAppendingString:@"?"];
        NSString *keyValue;
        NSString *moreUrl = @"&";
        for (NSString *key in param.allKeys) {
            if (key.length > 0) {
                NSString *value = [param objectForKey:key];
                if (moreUrl.length == 1) {
                    keyValue = [NSString stringWithFormat:@"%@=%@", key, value];
                }else{
                    keyValue = [NSString stringWithFormat:@"&%@=%@", key, value];
                }
                moreUrl = [moreUrl stringByAppendingString:keyValue];
            }
        }
        subUrl = [subUrl stringByAppendingString:moreUrl];
    }
    return subUrl;
}

+ (void)postImage:(UIImage *)photo withParameter:(NSDictionary *)params postURL:(NSString *)postUrl sender:(UIViewController *)vc authToken:(NSString *)token responseHandler:(void(^)(NSString * s3url, bool isSuccess))completionBlock {
    NSData *imageData =  UIImageJPEGRepresentation(photo, 0.5);

    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@/%@",BASE_URL, postUrl] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:UPLOAD_PHOTO_FILE_KEY fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } error:nil];
    if (token != nil) [request setValue:token forHTTPHeaderField:@"RevToken"];

    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    MBProgressHUD* hud;
    if(vc != nil) hud = [MBProgressHUD showHUDAddedTo:vc.view animated:YES];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if(vc != nil) [hud hideAnimated:YES];
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [vc showAlertView:nil message:NSLocalizedString(@"INTERNET_CONNECTION_ERROR", @"") handler:nil];
                          
                          completionBlock([responseObject deepMutableCopy], NO);
                          
                      } else {//SUCCESS
                          NSLog(@"ApiResult : %@ %@", response, responseObject);
                          NSString *status = [responseObject objectForKey:@"status"];
                          if([status isEqualToString:@"error"])
                          {
                              [vc showAlertView:nil message:[responseObject objectForKey:@"error"] handler:nil];
                          }
                          
                          completionBlock([responseObject deepMutableCopy], ([status isEqualToString:@"success"]));
                      }
                  }];
    
    [uploadTask resume];
}

+ (void)postVideo:(id)videoUrl withParameter:(NSDictionary *)params sender:(UIViewController *)vc authToken:(NSString *)token responseHandler:(void(^)(NSString * s3url, bool isSuccess))completionBlock {
    
    NSData *videoData = [NSData dataWithContentsOfFile:videoUrl];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@/%@",BASE_URL, @""] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:videoData name:UPLOAD_PHOTO_FILE_KEY fileName:@"video.mp4" mimeType:@"video/mp4"];
    } error:nil];
    if (token != nil) [request setValue:token forHTTPHeaderField:@"RevToken"];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    MBProgressHUD* hud;
    if(vc != nil) hud = [MBProgressHUD showHUDAddedTo:vc.view animated:YES];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if(vc != nil) [hud hideAnimated:YES];
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [vc showAlertView:nil message:NSLocalizedString(@"INTERNET_CONNECTION_ERROR", @"") handler:nil];
                          
                          completionBlock([responseObject deepMutableCopy], NO);
                          
                      } else { //SUCCESS
                          NSLog(@"ApiResult : %@ %@", response, responseObject);
                          NSString *status = [responseObject objectForKey:@"status"];
                          if([status isEqualToString:@"error"])
                          {
                              [vc showAlertView:nil message:[responseObject objectForKey:@"error"] handler:nil];
                          }
                          
                          completionBlock([responseObject deepMutableCopy], ([status isEqualToString:@"success"]));
                      }
                  }];
    
    [uploadTask resume];
    
}

+ (void)postDataToURL:(NSString *)endPoint withParameter:(NSDictionary *)params authToken:(nullable NSString *)token sender:(UIViewController *)vc responseHandler:(void(^)(id _Nullable responseObject, bool isSuccess))completionBlock {
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@/%@",BASE_URL, endPoint] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) { } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    MBProgressHUD* hud;
    if(vc != nil) hud = [MBProgressHUD showHUDAddedTo:vc.view animated:YES];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if(vc != nil) [hud hideAnimated:YES];
                      if (error) {
                          NSLog(@"Error: %@", error);
//                          [vc showAlertView:nil message:NSLocalizedString(@"INTERNET_CONNECTION_ERROR", @"") handler:nil];
                          
                          completionBlock([responseObject deepMutableCopy], NO);
                          
                      } else {
                          //SUCCESS
                          completionBlock([responseObject deepMutableCopy], YES);
                      }
                  }];
    
    [uploadTask resume];
}

+ (void)postJsonDataToURL:(NSString *)endPoint withParameter:(NSDictionary *)params authToken:(nullable NSString *)token sender:(UIViewController *)vc responseHandler:(void(^)(id _Nullable responseObject, bool isSuccess))completionBlock {
    
    NSMutableDictionary *params1 = [params convertToJSONSerializable];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@/%@", BASE_URL, endPoint] parameters:params1 error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    
    
    MBProgressHUD* hud;
    if(vc != nil) hud = [MBProgressHUD showHUDAddedTo:vc.view animated:YES];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if(vc != nil) [hud hideAnimated:YES];
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [vc showAlertView:nil message:NSLocalizedString(@"INTERNET_CONNECTION_ERROR", @"") handler:nil];
                          
                          completionBlock([responseObject deepMutableCopy], NO);
                          
                      } else {
                          //SUCCESS
                          NSLog(@"ApiResult : %@ %@", response, responseObject);
                          NSString *status = [responseObject objectForKey:@"status"];
                          
                          completionBlock([responseObject deepMutableCopy], ([status isEqualToString:@"success"]));
                      }
                  }];
    
    [uploadTask resume];
    
}

+ (void)getDataFromURL:(NSString *)endPoint withParameter:(NSDictionary *)params authToken:(nullable NSString *)token sender:(UIViewController *)vc responseHandler:(void(^)(id _Nullable responseObject, bool isSuccess))completionBlock {
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"%@/%@",BASE_URL, endPoint] parameters:params error:nil];
    if (token != nil) [request setValue:token forHTTPHeaderField:@"Token"];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;

    
    MBProgressHUD* hud;
    if(vc != nil) hud = [MBProgressHUD showHUDAddedTo:vc.view animated:YES];
    NSURLSessionDataTask *dataTask;
    dataTask = [manager
                dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                    if(vc != nil) [hud hideAnimated:YES];
                    if (error) {
                        NSLog(@"Error: %@", error);
                        [vc showAlertView:nil message:NSLocalizedString(@"INTERNET_CONNECTION_ERROR", @"") handler:nil];
                        
                        completionBlock([responseObject deepMutableCopy], NO);
                        
                    } else {
                        //SUCCESS
                        NSLog(@"ApiResult : %@ %@", response, responseObject);
                        NSString *status = [responseObject objectForKey:@"status"];
                        if([status isEqualToString:@"error"])
                        {
                            [vc showAlertView:nil message:[responseObject objectForKey:@"error"] handler:nil];
                        }
                        
                        completionBlock([responseObject deepMutableCopy], ([status isEqualToString:@"success"]));
                    }
                }];
    
    [dataTask resume];
    
}

+ (void)postStringToURL:(NSString *)endPoint withParameter:(NSDictionary *)params authToken:(nullable NSString *)token sender:(UIViewController *)vc responseHandler:(void(^)(id _Nullable responseObject, bool isSuccess))completionBlock {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    MBProgressHUD* hud;
    if(vc != nil) hud = [MBProgressHUD showHUDAddedTo:vc.view animated:YES];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    NSString *urlstr = [NSString stringWithFormat:@"%@/%@", BASE_URL, endPoint];
    [manager POST:urlstr parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if(vc != nil) [hud hideAnimated:YES];
        NSLog(@"[success]: %@",responseObject);
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        completionBlock([string deepMutableCopy], YES);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(vc != nil) [hud hideAnimated:YES];
        NSLog(@"[error]: %@", error);
        [vc showAlertView:nil message:NSLocalizedString(@"INTERNET_CONNECTION_ERROR", @"") handler:nil];
        completionBlock([error deepMutableCopy], NO);
    }];
    
}

+ (void)getStringFromURL:(NSString *)endPoint withParameter:(NSDictionary *)params authToken:(nullable NSString *)token sender:(UIViewController *)vc responseHandler:(void(^)(id _Nullable responseObject, bool isSuccess))completionBlock {
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"%@/%@",BASE_URL, endPoint] parameters:params error:nil];
    if (token != nil) [request setValue:token forHTTPHeaderField:@"Token"];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = NO;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    NSString *urlstr = [NSString stringWithFormat:@"%@/%@", BASE_URL, endPoint];

    [manager GET:urlstr parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"[success]: %@",responseObject);
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        completionBlock([string deepMutableCopy], YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"[error]: %@", error);
        [vc showAlertView:nil message:NSLocalizedString(@"INTERNET_CONNECTION_ERROR", @"") handler:nil];
        completionBlock([error deepMutableCopy], NO);
    }];
    
}

@end



