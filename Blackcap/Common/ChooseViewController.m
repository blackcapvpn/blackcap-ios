//
//  ChooseViewController.m
//  Blackcap
//
//  Created by JDev on 19/6/2017.
//  Copyright © 2017 JDev. All rights reserved.
//

#import "ChooseViewController.h"

@implementation ChooseViewController

+ (UIViewController *)loadViewControllerWithId:(NSString *)viewControllerId
{
    return [[ChooseViewController pickStoryboardForViewController:viewControllerId] instantiateViewControllerWithIdentifier:viewControllerId];
    
}

+ (UIStoryboard *)pickStoryboardForViewController:(NSString *)viewController
{
    NSBundle *locale = nil;
//    if ([[kUserDefaults objectForKey:USERDEFAULTS_LANGUAGE] isEqualToString:@"Español"]) {
//        locale = kLocaleBundle;
//    }
    
    if ([[ChooseViewController loginStoryboardArray] containsObject:viewController])
    {
        return [UIStoryboard storyboardWithName:@"Login" bundle:locale];
    }
    else if ([[ChooseViewController mainStoryboardArray] containsObject:viewController])
    {
        return [UIStoryboard storyboardWithName:@"Main" bundle:locale];
    }
    else if ([[ChooseViewController messageStoryboardArray] containsObject:viewController])
    {
        return [UIStoryboard storyboardWithName:@"Message" bundle:locale];
    }
    return [UIStoryboard storyboardWithName:@"" bundle:nil];
}

+ (NSArray *)loginStoryboardArray
{
    return @[@"LoginViewController",
             @"ForgotPasswordViewController",
             @"WebViewController",
             @"PrivacyVC",
             ];
}

+ (NSArray *)mainStoryboardArray
{
    return @[ @"BasicViewController",
              @"AdvancedViewController",
              @"SettingsViewController",
              @"MenuViewController",
              @"LogViewController",
              @"LegalViewController",
              @"PurchaseAlertVC",
              @"LanguageVC",
              ];
}

+ (NSArray *)messageStoryboardArray
{
    return @[ @"ConfirmPinViewController",
              @"ChangePinViewController",
              @"MessageHomeMenuViewController",
              @"MessengerSettingsViewController",
              @"ConversationSettingsViewController",
              @"OWSLinkedDevicesTableViewController",
              @"OWSLinkDeviceViewController",
              @"OWSQRCodeScanningViewController",
              @"InviteFriendsVC",
              ];
}

@end
