//
//  ApiManager.h
//  Monastery
//
//  Created by JDev on 14/2/2017.
//  Copyright © 2017 JDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ApiManager : NSObject

+ (void)postImage:(nonnull UIImage *)photo withParameter:(nullable NSDictionary *)params postURL:(nullable NSString *)postUrl sender:(nullable UIViewController *)vc authToken:(nullable NSString *)token responseHandler:(nullable void(^)(NSString *_Nullable s3url, bool isSuccess))completionBlock;

+ (void)postVideo:(nonnull id)videoUrl withParameter:(nullable NSDictionary *)params sender:(nullable UIViewController *)vc authToken:(nullable NSString *)token responseHandler:(nullable void(^)(NSString * _Nullable s3url, bool isSuccess))completionBlock;

+ (void)postDataToURL:(nonnull NSString *)endPoint withParameter:(nullable NSDictionary *)params authToken:(nullable NSString *)token sender:(nullable UIViewController *)vc responseHandler:(nullable void(^)(id _Nullable responseObject, bool isSuccess))completionBlock;

+ (void)getDataFromURL:(nonnull NSString *)endPoint withParameter:(nullable NSDictionary *)params authToken:(nullable NSString *)token sender:(nullable UIViewController *)vc responseHandler:(nullable void(^)(id _Nullable responseObject, bool isSuccess))completionBlock;

+ (void)postJsonDataToURL:(nonnull NSString *)endPoint withParameter:(nullable NSDictionary *)params authToken:(nullable NSString *)token sender:(nullable UIViewController *)vc responseHandler:(nullable void(^)(id _Nullable responseObject, bool isSuccess))completionBlock;

+ (void)postStringToURL:(nullable NSString *)endPoint withParameter:(nullable NSDictionary *)params authToken:(nullable NSString *)token sender:(nullable UIViewController *)vc responseHandler:(nullable void(^)(id _Nullable responseObject, bool isSuccess))completionBlock;

+ (void)getStringFromURL:(nullable NSString *)endPoint withParameter:(nullable NSDictionary *)params authToken:(nullable NSString *)token sender:(nullable UIViewController *)vc responseHandler:(nullable void(^)(id _Nullable responseObject, bool isSuccess))completionBlock;
@end
