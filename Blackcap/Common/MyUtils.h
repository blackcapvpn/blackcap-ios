//
//  MyUtils.h
//  Revent
//
//  Created by JDev on 13/2/2017.
//  Copyright © 2017 JDev. All rights reserved.
//
#import "BlackCapServer.h"

@interface MyUtils : NSObject

+ (BOOL)validateEmailWithString:(NSString*)email;

+ (NSString *)localizedString:(NSString *)key;
+ (int)getLanguage;
+ (void)addConstraintFromSubView:(UIView *)subView toParent:(UIView *)parent trailing:(NSNumber *)fTrailing leading:(NSNumber *)fLeading top:(NSNumber *)fTop bottom:(NSNumber *)fBottom width:(NSNumber *)fWidth height:(NSNumber *)fHeight;
+ (NSString *)getDateStringFromString:(NSString *)strDate fomart:(NSString *)format;
+ (NSString *)getLeftStringWithStartDate:(NSString *)startDate;
+ (NSInteger)getLeftSecondWithDate:(NSString*)strDate;
+ (NSString*)getElapsedTimeWithInterval:(NSTimeInterval)interval;

////////////////////////////
/////  My Animations  //////
+(void) fadeInView:(UIView*)view duration:(CGFloat)duration;
+(void) fadeOutView:(UIView*)view duration:(CGFloat)duration;

+(void)fadeInOut:(UIView*)view duration:(CGFloat)duration;
+(void)fadeOutIn:(UIView*)view duration:(CGFloat)duration;

+(void)scaleIn:(UIView*)view duration:(CGFloat)duration;
+(void)scaleOut:(UIView*)view duration:(CGFloat)duration;

/////////////////////////////////////////////////////////
/////  Save & Load Custom Object in NSUserDefaults  /////
+ (void)saveCustomObject:(BlackCapServer *)object key:(NSString *)key;
+ (BlackCapServer *)loadCustomObjectWithKey:(NSString *)key;
+ (void)saveCurrentServers:(id)object key:(NSString *)key;
+ (NSMutableArray *)loadCurrentWorkingServers:(NSString *)key;

@end
