//
//  MyUtils.m
//  Revent
//
//  Created by JDev on 13/2/2017.
//  Copyright © 2017 JDev. All rights reserved.
//

#import "MyUtils.h"

@implementation MyUtils

#pragma MARK - To validate Email-String
+ (BOOL)validateEmailWithString:(NSString*)email
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark - Method to show alert
+ (NSString *)localizedString:(NSString *)key {
    return NSLocalizedString(key, nil);
}
+ (int)getLanguage {
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    //  NSLog(@"language = %@", language);
    
    NSString *coreLang = [language substringToIndex:2];
    
    if([coreLang isEqualToString:@"zh"]) {
        return 1;
    } else {
        return 0;
    }
}
+ (void)addConstraintFromSubView:(UIView *)subView toParent:(UIView *)parent trailing:(NSNumber *)fTrailing leading:(NSNumber *)fLeading top:(NSNumber *)fTop bottom:(NSNumber *)fBottom width:(NSNumber *)fWidth height:(NSNumber *)fHeight{
    subView.translatesAutoresizingMaskIntoConstraints = NO;
    //Trailing
    if (fTrailing != nil) {
        NSLayoutConstraint *trailing =[NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeTrailing
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeTrailing
                                       multiplier:1.0f
                                       constant:[fTrailing floatValue]];
        [parent addConstraint:trailing];
    }
    //Leading
    if (fLeading != nil) {
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeLeft
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeLeft
                                       multiplier:1.0f
                                       constant:[fLeading floatValue]];
        [parent addConstraint:leading];
    }
    //Bottom
    if (fBottom != nil) {
        NSLayoutConstraint *bottom =[NSLayoutConstraint
                                     constraintWithItem:subView
                                     attribute:NSLayoutAttributeBottom
                                     relatedBy:NSLayoutRelationEqual
                                     toItem:parent
                                     attribute:NSLayoutAttributeBottom
                                     multiplier:1.0f
                                     constant:[fBottom floatValue]];
        [parent addConstraint:bottom];
    }
    //Top
    if (fTop != nil) {
        NSLayoutConstraint *top =[NSLayoutConstraint
                                     constraintWithItem:subView
                                     attribute:NSLayoutAttributeTop
                                     relatedBy:NSLayoutRelationEqual
                                     toItem:parent
                                     attribute:NSLayoutAttributeTop
                                     multiplier:1.0f
                                     constant:[fTop floatValue]];
        [parent addConstraint:top];
    }
    //Height to be fixed for SubView same as AdHeight
    if (fHeight != nil) {
        NSLayoutConstraint *height = [NSLayoutConstraint
                                      constraintWithItem:subView
                                      attribute:NSLayoutAttributeHeight
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                      multiplier:1.0
                                      constant:[fHeight floatValue]];
        [subView addConstraint:height];
    }
    //Width to be fixed for SubView same as AdHeight
    if (fWidth != nil) {
        NSLayoutConstraint *width = [NSLayoutConstraint
                                      constraintWithItem:subView
                                      attribute:NSLayoutAttributeWidth
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                      multiplier:1.0
                                      constant:[fWidth floatValue]];
        [subView addConstraint:width];
    }
}
+ (NSString *)getDateStringFromString:(NSString *)strDate fomart:(NSString *)format {
    /// @"M/d/yy @ hh a", @"dd,MMM", @"M/d/yy @ hh a", @"EEEE d, MMM yyyy, h:mm a"
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //    [formatter setTimeStyle:NSDateFormatterMediumStyle];
    NSDate *date = [formatter dateFromString:strDate];
    
    [formatter setDateFormat:format];
    NSString *shortDate = [formatter stringFromDate:date];
    return shortDate;
}

+ (NSString *)getLeftStringWithStartDate:(NSString *)startDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //    [formatter setTimeStyle:NSDateFormatterMediumStyle];
    NSDate *date = [formatter dateFromString:startDate];
    NSDate *today = [NSDate date];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitMinute fromDate:today toDate:date options:0];
    NSInteger totalMinutes = [components minute];
    NSInteger day = totalMinutes/(24*60);
    NSInteger hour = (totalMinutes - day * 1440)/60;
    NSInteger minute = totalMinutes - day * 1440 - hour * 60;
    NSString *left = [NSString stringWithFormat:@"%ldd %ldh %ldm Left", (long)day, (long)hour, (long)minute];
    if (totalMinutes < 0) {
        return nil;
    }
    return left;
}

+ (NSInteger)getLeftSecondWithDate:(NSString*)strDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:strDate];
    NSDate *today = [NSDate date];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitMinute fromDate:today toDate:date options:0];
    NSInteger totalSeconds = [components minute] * 60;
    return totalSeconds;
}

+(void) fadeInView:(UIView*)view duration:(CGFloat)duration{
    [view setAlpha:0];
    [UIView beginAnimations:NULL context:nil];
    [UIView setAnimationDuration:duration];
    [view setAlpha:1];
    [UIView commitAnimations];
}

+(void) fadeOutView:(UIView*)view duration:(CGFloat)duration{
    [UIView beginAnimations:NULL context:nil];
    [UIView setAnimationDuration:duration];
    [view setAlpha:0];
    [UIView commitAnimations];
}

+(void)fadeInOut:(UIView*)view duration:(CGFloat)duration{
    
    [view setAlpha:0.0f];
    //fade in
    [UIView animateWithDuration:duration animations:^{
        [view setAlpha:1.0f];
    } completion:^(BOOL finished) {
        //fade out
        [UIView animateWithDuration:duration animations:^{
            [view setAlpha:0.0f];
        } completion:nil];
    }];
}

+(void)fadeOutIn:(UIView*)view duration:(CGFloat)duration{
    
    [view setAlpha:1.0f];
    //fade in
    [UIView animateWithDuration:duration animations:^{
        [view setAlpha:0.0f];
    } completion:^(BOOL finished) {
        //fade out
        [UIView animateWithDuration:duration animations:^{
            [view setAlpha:1.0f];
        } completion:nil];
    }];
}

+(void)scaleIn:(UIView*)view duration:(CGFloat)duration{
    [view setTransform:CGAffineTransformMakeScale(0.01, 0.01)];
    [UIView animateWithDuration:duration animations:^{
        [view setTransform:CGAffineTransformMakeScale(1, 1)];
    }];
}

+(void)scaleOut:(UIView*)view duration:(CGFloat)duration{
    [view setTransform:CGAffineTransformMakeScale(1, 1)];
    [UIView animateWithDuration:duration animations:^{
        [view setTransform:CGAffineTransformMakeScale(0.01, 0.01)];
    }];
}

+ (void)saveCustomObject:(BlackCapServer *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    [kUserDefaults setObject:encodedObject forKey:key];
    [kUserDefaults synchronize];
    
}

+ (BlackCapServer *)loadCustomObjectWithKey:(NSString *)key {
    if (![kUserDefaults objectForKey:key]) {
        return nil;
    }
    NSData *encodedObject = [kUserDefaults objectForKey:key];
    BlackCapServer *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

+ (void)saveCurrentServers:(id)object key:(NSString *)key {
    [kUserDefaults setObject:object forKey:key];
    [kUserDefaults synchronize];
}

+ (NSMutableArray *)loadCurrentWorkingServers:(NSString *)key {
    if (![kUserDefaults objectForKey:key]) {
        return nil;
    }
    NSMutableArray *servers = [[NSMutableArray alloc] init];
    id workers = [[kUserDefaults objectForKey:key] objectForKey:@"workers"];
    if ([workers count] > 0) {
        for (id worker in workers) {
            BlackCapServer *server = [[BlackCapServer alloc] initWithDictionary:worker];
            if (YES) {
//            if (server.up) {
                [servers addObject:server];
            }
        }
    }
    return servers;
}

+ (NSString*)getElapsedTimeWithInterval:(NSTimeInterval)interval {
    NSString *result = @"00:00";
    // Divide the interval by 3600 and keep the quotient and remainder
    div_t h = div(interval, 3600);
    int hours = h.quot;
    // Divide the remainder by 60; the quotient is minutes, the remainder
    // is seconds.
    div_t m = div(h.rem, 60);
    NSString *minutes = m.quot < 10 ? [NSString stringWithFormat:@"0%d", m.quot] : [NSString stringWithFormat:@"%d", m.quot];
    NSString *seconds = m.rem < 10 ? [NSString stringWithFormat:@"0%d", m.rem] : [NSString stringWithFormat:@"%d", m.rem];
    
    // If you want to get the individual digits of the units, use div again
    // with a divisor of 10.
    
    if (hours > 0) {
        result = [NSString stringWithFormat:@"%d:%@:%@", hours, minutes, seconds];
    } else {
        result = [NSString stringWithFormat:@"%@:%@", minutes, seconds];
    }
    return result;
    
}
@end
