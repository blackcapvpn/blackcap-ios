//
//  Constant.h
//  Revent
//
//  Created by JDev on 14/2/2017.
//  Copyright © 2017 JDev. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define APPNAME             @"Black Cap"

#define kUserDefaults       [[NSUserDefaults alloc] initWithSuiteName:BlackCapVPNAppGroupsName]

#define kDeviceHeight       [[UIScreen mainScreen] bounds].size.height
#define kDeviceWidth        [[UIScreen mainScreen] bounds].size.width
#define kSharedAppdelegate  (AppDelegate*)[[UIApplication sharedApplication] delegate]
//#define kLocaleBundle       [(AppDelegate*)[[UIApplication sharedApplication] delegate] localeBundle]
#define kLocaleBundle       [kSharedAppdelegate localeBundle]

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define LocalizedStringFromBundle(bundle, key, comment) NSLocalizedStringFromTableInBundle(key, nil, bundle, comment)

//Urls
//NSUserDefaults
#define USERDEFAULTS_USERNAME                       @"username"
#define USERDEFAULTS_PASSWORD                       @"password"
#define USERDEFAULTS_LOGIN_STATUS_VPN               @"vpn login status"
#define USERDEFAULTS_LOGIN_STATUS_MESSENGER         @"messenger login status"
#define USERDEFAULTS_FREE_MESSAGE_NUM               @"message number"
#define USERDEFAULTS_FREE_MESSAGE_DATE              @"message date"

#define USERDEFAULTS_DNS_LEAK_PROTECTION            @"DNS leak protection"
#define USERDEFAULTS_AUTO_RECONNECT                 @"Auto Reconnect"
#define USERDEFAULTS_SHOW_CONNECTION_NOTIFICATION   @"Show connection notification"

#define USERDEFAULTS_CONFIGURED_SERVER              @"CurrentConfiguredServer"
#define USERDEFAULTS_CURRENT_WORKING_SERVERS        @"CurrentWorkingServers"
#define USERDEFAULTS_TODAY_CONFIGURING_SERVER       @"TodayConfiguringServer"
#define USERDEFAULTS_TODAY_URL                      @"TodayWidgetURL"
#define USERDEFAULTS_PING_TIMES                     @"PingTimes"

#define USERDEFAULTS_PIN                            @"pin_number"

#define USERDEFAULTS_CARD_NUMBER                    @"cardnumber"
#define USERDEFAULTS_CARD_EXPDATE                   @"expdate"
#define USERDEFAULTS_CARD_CVV                       @"cvv"

#define USERDEFAULTS_LANGUAGE                       @"language"

typedef enum : NSUInteger {
    VPN_CONNECTED,
    VPN_CONNECTING,
    VPN_DISCONNECTED,
    VPN_DISCONNECTING,
    VPN_REASSERTING,
    VPN_INVALID
} CONNECT_STATE;

// For AFNetworking
#define BASE_URL                            @"https://blackcap.zone"

#define SUB_URL_PASSWORD_RESET              @"members/pwreset.php"

#define SUB_URL_LOGIN                       @"api/login.php"
#define SUB_URL_SERVER_LIST                 @"api/status-list.php"
#define SUB_URL_WORKER_LIST                 @"api/worker-list.php"
#define SUB_URL_DNS_LIST                    @"api/dns-list.php"
#define SUB_URL_OVPN_CONFIG                 @"api/openvpn-config.php?s="
#define SUB_URL_UPGRADE_PURCHASE            @"api/upgrade-product.php"
#define SUB_URL_UPDATE_MESSAGE_COUNT        @"api/message-sent.php"


#define SUB_URL_OPENVPN_CONNECT             @"ovpn/.ovpn"

#define UPLOAD_PHOTO_TYPE                   @"type"
#define UPLOAD_PHOTO_FILE_KEY               @"photo"
#define MYDATE_FORMAT                       @"dd MMM, yyyy"
#define MYDATE_LOCALE                       [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]

#define BlackCapVPNAppGroupsName            @"group.zone.blackcap.vpn"
#define TunnelProviderBundleID              @"zone.blackcap.vpn.tunnel-provider"
#define ConfigurationKeyFileContent         @"zone.blackcap.vpn.configuration-key.file-content"

#endif /* Constant_h */
