//
//  Colors.h
//  Blackcap
//

#ifndef Colors_h
#define Colors_h


#define COLOR(k) [UIColor colorWithRed:(k)/255.0 green:(k)/255.0 blue:(k)/255.0 alpha:1]

#define kColorPrimaryGray           COLOR(33)
#define kColorSecondaryGray         COLOR(114)
#define kColorNewGray               COLOR(66)
#define kColorBorder                COLOR(182)
#define kColorChat                  COLOR(224)
#define kColorGray200               COLOR(200)
#define KColorBackground            COLOR(238)

#define kColorRed                   [UIColor colorWithRed:221.0/255.0f green:110.0/255.0f blue:117.0/255.0f alpha:1]
#define kColorGreen                 [UIColor colorWithRed:108.0/255.0f green:196.0/255.0f blue:136.0/255.0f alpha:1]

#define kColorPrimaryBlue       [UIColor colorWithRed:48.0/255.0 green:66.0/255.0 blue:153.0/255.0 alpha:1]

#define kColorHeaderBlue        [UIColor colorWithRed:48.0/255.0 green:66.0/255.0 blue:153.0/255.0 alpha:1]
//#define kColorHeaderBlue        [UIColor colorWithRed:63.0/255.0 green:81.0/255.0 blue:181.0/255.0 alpha:1]
#define kColorLightBlue         [UIColor colorWithRed:197.0/255.0 green:202.0/255.0 blue:230.0/255.0 alpha:1]
#define kColorButtonBlue        [UIColor colorWithRed:68.0/255.0 green:138.0/255.0 blue:255.0/255.0 alpha:1]




#endif /* Colors_h */
