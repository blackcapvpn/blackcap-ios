//
//  MyAlertController.m
//  Blackcap
//
//  Created by JDev on 5/12/2017.
//  Copyright © 2017 rstar. All rights reserved.
//

#import "MyAlertController.h"

@interface MyAlertController ()

@end

@implementation MyAlertController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    //set this to whatever color you like...
    self.view.tintColor = [UIColor blackColor];
}

@end

