//
//  UIViewController+Alert.h
//  CloudPhoto
//
//  Created by Xiaoping Zheng on 3/13/16.
//  Copyright © 2016 James W. All rights reserved.
//

#import <UIKit/UIKit.h>
//typedef void (^DownloadProgressBlock)(int64_t totalSize, int64_t downloadedSize);
typedef void (^HandlerBlock)(UIAlertAction * _Nonnull action);
@interface UIViewController(Alert)
- (void) showAlertView:(NSString *_Nullable)title message:(NSString * _Nullable)message handler:(nullable HandlerBlock)handler;
- (void) showAlertView:(NSString *_Nullable)title message:(NSString * _Nullable)message button:(NSString * _Nullable)button handler:(nullable HandlerBlock)handler;
- (void) showAlertView:(NSString *_Nullable)title message:(NSString * _Nullable)message okButton:(NSString * _Nullable)okTitle cancelButton:(NSString * _Nullable)cancelTitle okHandler:(nullable HandlerBlock)okHandler cancelHandler:(nullable HandlerBlock)cancelHandler;
@end
