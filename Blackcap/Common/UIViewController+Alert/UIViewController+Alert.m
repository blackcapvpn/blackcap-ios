//
//  UIViewController+Alert.m
//  CloudPhoto
//
//  Created by Xiaoping Zheng on 3/13/16.
//  Copyright © 2016 James W. All rights reserved.
//

#import "UIViewController+Alert.h"
#import "Constant.h"

@implementation UIViewController(Alert)

- (void) showAlertView:(NSString*)title message:(NSString*)message handler:(HandlerBlock)handler
{
    [self showAlertView:title message:message button:@"OK" handler:handler];
}

- (void) showAlertView:(NSString *_Nullable)title message:(NSString * _Nullable)message button:(NSString * _Nullable)button handler:(nullable HandlerBlock)handler
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    [alert.view setTintColor:[UIColor blackColor]];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:button
                               style:UIAlertActionStyleDefault
                               handler:handler];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) showAlertView:(NSString *_Nullable)title message:(NSString * _Nullable)message okButton:(NSString * _Nullable)okTitle cancelButton:(NSString * _Nullable)cancelTitle okHandler:(nullable HandlerBlock)okHandler cancelHandler:(nullable HandlerBlock)cancelHandler
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    [alert.view setTintColor:[UIColor blackColor]];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:okTitle
                               style:UIAlertActionStyleDefault
                               handler:okHandler];
    UIAlertAction *cancelButton = [UIAlertAction
                                   actionWithTitle:cancelTitle
                                   style:UIAlertActionStyleDefault
                                   handler:cancelHandler];
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
@end
