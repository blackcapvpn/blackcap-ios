//
//  NSDate+ISO8601.h
//  PupKit
//
//  Created by Xiaoping Zheng on 5/8/16.
//  Copyright © 2016 James W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (ISO8601)

+ (NSDate *)getDateFromString:(NSString *)dateString;
+ (NSString *)getStringFromDate:(NSDate *)date;

+ (NSString *)getLongDateStringFromDate:(NSDate *)date;
+ (NSString *)getSimpleDateStringFromDate:(NSDate *)date;
+ (NSString *)getShortDateStringFromDate:(NSDate *)date;
+ (NSString *)getShortTimeStringFromDate:(NSDate *)date;

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSInteger)monthsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

+ (NSDate *)getDateFromYear:(long)year month:(long)month day:(long)day hour:(long)hour minute:(long)minute;
+ (NSDate *)getNextDayFromDate:(NSDate *)fromDate dayToAdd:(long)days;
+ (NSDate *)setDateHour:(long)hour minute:(long)minute dateToBeModified:(NSDate*)date;

//for adoption
+ (NSString *)getAgeTextFromDateString:(NSString *)dateString;
+ (NSString *)getAgeTextFromDate:(NSDate *)date;

//self methods
+ (NSDate *)dateMyTimezone;
+ (NSDate *) toLocalTime:(NSDate*) utcTime;
+ (NSDate *) toGlobalTime:(NSDate*) localTime;
@end
