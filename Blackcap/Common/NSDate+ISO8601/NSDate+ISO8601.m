//
//  NSDate+ISO8601.m
//  PupKit
//
//  Created by Xiaoping Zheng on 5/8/16.
//  Copyright © 2016 James W. All rights reserved.
//

#import "NSDate+ISO8601.h"

@implementation NSDate (ISO8601)
+ (NSDate *)getDateFromString:(NSString *)dateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    // Always use this locale when parsing fixed format date strings
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:posix];
    NSDate *date = [formatter dateFromString:dateString];
    
    return date;
}

+ (NSString *)getStringFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    // Always use this locale when parsing fixed format date strings
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:posix];
    NSString *dateString = [formatter stringFromDate:date];

    return dateString;
}

+ (NSString *)getLongDateStringFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+ (NSString *)getSimpleDateStringFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

+ (NSString *)getShortDateStringFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    [formatter setDateFormat:@"dd MMM"];
    return [formatter stringFromDate:date];
}

+ (NSString *)getShortTimeStringFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    [formatter setDateFormat:@"HH:mm:ss"];
    return [formatter stringFromDate:date];
}

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    if(fromDateTime == nil || toDateTime == nil) {
        return 0;
    }
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

+ (NSInteger)monthsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    if(fromDateTime == nil || toDateTime == nil) {
        return 0;
    }
    
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitMonth
                                       fromDate:fromDateTime
                                       toDate:toDateTime
                                       options:0];
    
    //NSString *age = [NSString stringWithFormat:@"%@ Month(s)",[@([ageComponents month]) stringValue]];
    return [ageComponents month];
}

+ (NSDate *)getDateFromYear:(long)year month:(long)month day:(long)day hour:(long)hour minute:(long)minute {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [calendar setTimeZone:[NSTimeZone defaultTimeZone]];
    [components setSecond:0];
    [components setMinute:minute];
    [components setHour:hour];
    [components setDay:day];
    [components setMonth:month];
    [components setYear:year];
    return [calendar dateFromComponents:components];
}

+ (NSDate *)getNextDayFromDate:(NSDate *)fromDate dayToAdd:(long)days {
    return [fromDate dateByAddingTimeInterval:60*60*24*days];
}

+ (NSDate *)setDateHour:(long)hour minute:(long)minute dateToBeModified:(NSDate*)date {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMinute | NSCalendarUnitHour | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    long year = [components year];
    long month = [components month];
    long day = [components day];
    
    return [NSDate getDateFromYear:year month:month day:day hour:hour minute:minute];
}


+ (NSString *)getAgeTextFromDateString:(NSString *)dateString {
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy/MM/dd"];
    
    NSDateFormatter *formatter2_1 = [[NSDateFormatter alloc] init];
    [formatter2_1 setDateFormat:MYDATE_FORMAT];
    [formatter2_1 setTimeZone:[NSTimeZone defaultTimeZone]];
    
    
    NSDate *birthday = [formatter1 dateFromString:dateString];
    if(birthday == nil) {
        birthday = [formatter2_1 dateFromString:dateString];
    }
    
    
    NSString *result = @"";
    if(birthday != nil) {
        result = [NSDate getAgeTextFromDate:birthday];
    } else {
        result = @"";
    }
    
    //result = [result stringByAppendingString:@" old"];
    
    return result;
}

+ (NSString *)getAgeTextFromDate:(NSDate *)date {
    long months = [NSDate monthsBetweenDate:date andDate:[NSDate date]];
    
    NSString *result = @"";
    if(months == 0) {
        long days = [NSDate daysBetweenDate:date andDate:[NSDate date]];
        result = [NSString stringWithFormat:@"%ld %@", days, [MyUtils localizedString:@"DAY"]];
        if(days > 1 && [MyUtils getLanguage] == 0) {
            result = [result stringByAppendingString:@"s"];
        }
        
    } else if(months <= 12) {
        result = [NSString stringWithFormat:@"%ld %@", months, [MyUtils localizedString:@"MONTH"]];
        if(months > 1 && [MyUtils getLanguage] == 0) {
            result = [result stringByAppendingString:@"s"];
        }
    } else {
        long years = months / 12;
        months = months % 12;
        
        result = [NSString stringWithFormat:@"%ld %@", years, [MyUtils localizedString:@"YEAR"]];
        if(years > 1 && [MyUtils getLanguage] == 0) {
            result = [result stringByAppendingString:@"s"];
        }
        
        if(months > 0) {
            result = [result stringByAppendingString:[NSString stringWithFormat:@" %ld %@", months, [MyUtils localizedString:@"MONTH"]]];
            if(months > 1 && [MyUtils getLanguage] == 0) {
                result = [result stringByAppendingString:@"s"];
            }
        }
    }
    
    return result;
}

+ (NSDate *)dateMyTimezone {
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* nowDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    return nowDate;
}

+ (NSDate *) toLocalTime:(NSDate*) utcTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: utcTime];
    return [NSDate dateWithTimeInterval: seconds sinceDate: utcTime];
}

+ (NSDate *) toGlobalTime:(NSDate*) localTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: localTime];
    return [NSDate dateWithTimeInterval: seconds sinceDate: localTime];
}


@end
