//
//  Fonts.h
//  Blackcap
//

#ifndef Fonts_h
#define Fonts_h

#define AppRegularFont(fontSize) [UIFont fontWithName:@"Roboto-Regular" size:fontSize]
#define SystemBoldFont(fontSize) [UIFont fontWithName:@"Helvetica-Bold" size:fontSize]

#define AppCondensedFont(fontSize) [UIFont fontWithName:@"RobotoCondensed-Regular" size:fontSize]
#define AppSemiboldFont(fontSize) [UIFont fontWithName:@"ProximaNova-Semibold" size:fontSize]
#define AppLightFont(fontSize) [UIFont fontWithName:@"ProximaNova-Regular" size:fontSize]

#define AppNavbarFont                   AppRegularFont(16)
#define AppTextFieldFont                AppRegularFont(14)


#endif /* Fonts_h */
