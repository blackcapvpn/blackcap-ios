//
//  MyDeepCopy.h
//  PupKit
//
//  Created by Xiaoping Zheng on 5/8/16.
//  Copyright © 2016 James W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject(MyDeepCopy)
-(id)deepMutableCopy;
-(id)convertToJSONSerializable;
-(id)convertFromJSONSerializable;
@end
