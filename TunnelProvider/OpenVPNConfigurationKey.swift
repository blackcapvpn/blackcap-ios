//
//  OpenVPNKey.swift
//  OpenVPN Adapter
//
//  Created by rstar on 09.07.17.
//
//

import Foundation

struct OpenVPNConfigurationKey {
    
    static let fileContent = "zone.blackcap.vpn.configuration-key.file-content"
    static let username = "zone.blackcap.vpn.configuration-key.username"
    static let password = "zone.blackcap.vpn.configuration-key.password"
    
}
