# Black Cap for iOS

Black cap is a VPN and Messaging app for private network and simple private communication.
It includes the OpenVPN and Signal.

[![Available on the App Store](http://cl.ly/WouG/Download_on_the_App_Store_Badge_US-UK_135x40.svg)](https://itunes.apple.com/us/app/black-cap/id1453048780?ls=1&mt=8)


## Building

As of this writing, that's Xcode 10.1

### 1. Clone

Clone the repo to a working directory:

```
git clone git@bitbucket.org:privacyguardx/blackcap-ios.git
```

We recommend you use `git clone`, rather than downloading a prepared zip file.


### 2. Pod Install

```
cd blackcap-ios
```

You need to install pod libraries:

```
pod install
```

git submodule init

### 3. Xcode

Open the `Blackcap.xcworkspace` in Xcode.

```
open Blackcap.xcworkspace
```

In the TARGETS area of the General tab, change the Team drop down to
your own. You will need to do that for all the listed targets, for ex. 
Blackcap, TodayWidget, OpenVPN Adapter and TunnelProvider. You will need an Apple Developer account for this. 

Build and Run and you are ready to go!


## License

Copyright ©<2018><Black Cap VPN SAS>

Exclusion to GNU GPL V3 licence terms derived from GPL3 s7
